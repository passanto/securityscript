# UTF8
[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8
Set-ExecutionPolicy Bypass Process -Force

$na=Get-NetAdapter
if (!$na){
	write-Host -foregroundColor red "No network adapter found!"
	$continue=read-host "Continue?"
	if (! ($continue -like 'Y')){
		#Read-Host "Press enter to exit..."
		exit
	}
}
# folders variable:
$ScriptsFolder=(get-item -path "import\Scripts").fullname
$ConfFolder=(get-item -path "import\Conf").fullname
$ActionsScriptFolder=(get-item -path "import\Scripts\actions").fullname
$ThirdPartyInstallerFolder=(get-item -path "import\Softwares\installers").fullname
$PortUpFolder=(get-item -path "import\Softwares\PortUp").fullname
$UtilsFolder=(get-item -path "import\Softwares\utils").fullname
$UtilsFolderRelPath=".\import\Softwares\utils"
$InstallersFolder=(get-item -path "import\Softwares\installers").fullname
$OutputsFolder=(get-item -path "outputs").fullname


# copy default security script if required
If (-Not (Test-Path "$ConfFolder\securityScript.properties")){
	$DefaultConfFolder=(get-item -path "import\Conf\default").fullname
	copy-item -path "$DefaultConfFolder\securityScript.properties" -destination $ConfFolder
}
$secProps = convertfrom-stringdata (get-content $ConfFolder\securityScript.properties -raw)
#$secPropsWarningEnabled=$secProps.whitelisting_warning
$secPropsProcessFile=$secProps.process_file

# import functions script
. $ScriptsFolder\functions.ps1
. $ScriptsFolder\actionsFunctions.ps1
. $ScriptsFolder\srpFunctions.ps1
. $ScriptsFolder\firewalls.ps1


if (($secPropsProcessFile -eq "0")){
	# show menu
	$exitMainMenu="N"
	while($exitMainMenu -like 'N'){
		write-host -foregroundcolor yellow "1) Run full admin process"
		#write-host -foregroundcolor yellow "2) Find paths to blacklist"
		#write-host -foregroundcolor yellow "3) Show blacklist rules"
		write-host -foregroundcolor yellow "2) SRP menu"
		write-host -foregroundcolor yellow "3) Check Firewall rules"
		write-host -foregroundcolor yellow "4) EXIT"
		$menuChoice = read-host
		switch($menuChoice){
			1{
				$secPropsProcessFile="1"
				$exitMainMenu = 'Y'
				break
			}
			#2{
			#	findPathToDeny
			#}
			#3{
			#	listCurrentSrpRules
			#}
			2{
				srpMenu
			}
			3{
				checkFirewallRules
			}
			4{
				exit
			}
			default{
				write-host "Invalid choice, try again"
			}
		}
	}
}


# error log file
$errorLogFile=$OutputsFolder+"\errors.txt"
if (! (test-path $errorLogFile)){
	new-item $errorLogFile | out-null
}
$errorLogFileItem = get-item $errorLogFile
$errorLogFileName=$errorLogFileItem.fullname
write-host -foregroundcolor yellow "Any anomalous event will be logged in the '$errorLogFileName' file "
write-host
read-host "Press Enter to continue..."



<#
load properties for admin process
#>
if ($secPropsProcessFile -eq "1" -or $secPropsProcessFile -eq 1){
	. $ScriptsFolder\puFunctions.ps1

	$portableUpdateRequired=$secProps.portable_update_required

	$portableUpdateInitialized=$secProps.portable_update_intialized
	$restartedPostPU=$secProps.restart_due_to_pu
	if ($restartedPostPU -like "false"){
		$restartedPostPU=$false
	} else {
		$restartedPostPU=$true
	}
	$portableUpdateDownloaded=$secProps.portable_update_downloaded
	$portableUpdateCompleted=$secProps.portable_update_completed
	#$systemDisconnected=$secProps.system_disconnected
	$externalSystemAvailable=$secProps.external_system_available
	#echo secPropsProcessFile=$secPropsProcessFile

}


#if(([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")){
$amIadmin=whoami /groups /fo csv | convertfrom-csv | where-object { $_.SID -eq "S-1-5-32-544" }
if ($amIadmin){
	if(([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")){
		# administrators' scripts
		if (checkAutorunTask){
			removeAutorunTask
		}
		$secScriptFileName="securityScript$secPropsProcessFile.ps1"
		write-host "Launching securityScript$secPropsProcessFile.ps1"
		runScriptFile $secScriptFileName
	} else {
		write-host -foregroundcolor red "Please execute 'as Administrator'..."
		#read-host "Press any key to exit..."
		#exit
	}
} else {
	# Users' script
	if ($secPropsProcessFile -eq "6") {
		write-host "Launching securityScript5.ps1"
		.\Import\Scripts\actions\user\securityScript5.ps1
	}
}
