function srpMenu(){
	$exitMenu="N"
	$ScriptsFolder=(get-item -path "import\Scripts").fullname
	$ConfFolder=(get-item -path "import\Conf").fullname
	$ActionsScriptFolder=(get-item -path "import\Scripts\actions").fullname
	$ThirdPartyInstallerFolder=(get-item -path "import\Softwares\installers").fullname
	$PortUpFolder=(get-item -path "import\Softwares\PortUp").fullname
	$UtilsFolder=(get-item -path "import\Softwares\utils").fullname
	$InstallersFolder=(get-item -path "import\Softwares\installers").fullname
	$OutputsFolder=(get-item -path "outputs").fullname
	while($exitMenu -notlike 'Y'){
		write-host -foregroundcolor yellow "--------------------------------"
		write-host -foregroundcolor yellow "1) List rules"
		write-host -foregroundcolor yellow "2) Initialize and configure SRP"
		write-host -foregroundcolor yellow "3) Find paths to blacklist"
		write-host -foregroundcolor yellow "4) Lock Policies"
		write-host -foregroundcolor yellow "5) Unlock Policies"
		write-host -foregroundcolor yellow "6) Update GUI Policies"
		write-host -foregroundcolor yellow "7) EXIT"
		$menuChoice = read-host
		switch($menuChoice){
			1{
				listCurrentSrpRules
			}
			2{
				initializeSRP
			}
			3{
				findPathToDeny
			}
			4{
				lockSRP
			}
			5{
				unlockSRP
			}
			6{
				checkSrpKeys
			}
			7{
				write-host -foregroundcolor yellow "Thank you for using."
				write-host "Press ENTER to exit..."
				read-host
				$exitMenu='Y'
			}
			default{
				write-host "Invalid choice, try again"
			}
		}
	}
}

#?HKLM:\SOFTWARE\WOW6432Node\Policies\Microsoft\Windows\Safer
$saferKeyPath="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer"
$leveKeyPath="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers"
$leveKey="DefaultLevel"
$denyLevelValue=0
$allowLevelValue=262144

$deniedPathParentKey="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers\0\Paths"
$allowedPathParentKey="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers\262144\Paths"
$allowedHashParentKey="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers\262144\Hashes"

$deniedConfFile="$ConfFolder\SRP\srpDeniedPaths.txt"
$allowedConfFile="$ConfFolder\SRP\srpAllowedPaths.txt"
$systemSaferPath="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer"






function checkSrpKeys(){
	$Source=(gi $saferKeyPath).pspath
	$sourceName=(gi $saferKeyPath).name
	#$sourceKeys = Get-ChildItem $Source -Recurse
	$destinationKeys=@()
	$guiGPOs = getAllGuiGPO #HKEY_CURRENT_USER
	Foreach ($guiGPO in $guiGPOs){
		$destinationKeys+=$guiGPO
	}
	$sidGPOs = getAllSidGPO #HKEY_CURRENT_USER
	Foreach ($guiGPO in $sidGPOs){
		$destinationKeys+=$guiGPO
	}

	Foreach ($destKey in $destinationKeys)
	{
		if (! (test-path $destKey)){
			new-item $destKey -force | out-null
		}
		$destination=(gi $destKey).name
		$ModifiedDestination = ($Source).replace($sourceName,$destination)
    if (test-path "$ModifiedDestination\Safer\CodeIdentifiers"){
	    remove-item "$ModifiedDestination\Safer\CodeIdentifiers" -force -recurse
    }
		#write-host "Copying $Source"
		#write-host "Into  $ModifiedDestination"
		Copy-Item $Source -Destination $ModifiedDestination -Recurse -Force
	}

	<#$Source=(gi $saferKeyPath).pspath
	$sourceName=(gi $saferKeyPath).name
	$destinationKeys=@()
	$allowedPSPathParentKey=((gi $allowedPathParentKey -erroraction silentlycontinue)).pspath
	$deniedPSPathParentKey=((gi $deniedPathParentKey -erroraction silentlycontinue)).pspath
	clear-Content "$ConfFolder\SRP\srpAllowedPaths.txt"
	clear-Content "$ConfFolder\SRP\srpDeniedPaths.txt"
	# copy Safer key value into ALL guis
	$guiGPOs = getAllGuiGPO #HKEY_CURRENT_USER
	Foreach ($guiGPO in $guiGPOs){
		$destinationKeys+=$guiGPO
	}

	$backedUpAllowedPath=$false
	$backedUpDeniedPath=$false
	Foreach ($destKey in $destinationKeys)
	{
		$destSaferKey = "$destKey\Safer"
		if (test-path $destSaferKey){
			$destination=(gi $destSaferKey).name
			$sourceKeys = Get-ChildItem $Source -Recurse
			ForEach ($sourceKey in $sourceKeys){
				$ModifiedDestination = ($sourceKey.PSParentPath).replace("$sourceName","$destination")
				#$index=$ModifiedDestination.LastIndexOf("\")
				$parentDestination=$ModifiedDestination#.Substring(0,$index)
				if ( ($sourceKey.pspath -eq $allowedPSPathParentKey) -and ($backedUpAllowedPath -eq $false) ){
				#write-host "FOUND THE ALLOWED PATH KEY for $allowedPSPathParentKey"
					foreach ($i in (gci ($sourceKey.pspath)) ){
						$allowedPath = Get-ItemPropertyValue $i.pspath -name itemdata
						#read-host "allowedPath: $allowedPath"
						#Add-Content "$ConfFolder\SRP\srpAllowedPaths.txt" $allowedPath
						$backedUpAllowedPath=$true
					}
				}
				if ( ($sourceKey.pspath -eq $deniedPSPathParentKey) -and ($backedUpDeniedPath -eq $false) ){
				#write-host "FOUND THE ALLOWED PATH KEY for $deniedPSPathParentKey"
					foreach ($i in (gci ($sourceKey.pspath)) ){
						$deniedPath = Get-ItemPropertyValue $i.pspath -name itemdata
						#read-host "allowedPath: $deniedPath"
						Add-Content "$ConfFolder\SRP\srpAllowedPaths.txt" $deniedPath
						$backedUpDeniedPath=$true
					}
				}
				if (test-path $parentDestination){
					remove-item $parentDestination -force -recurse
				}
				Copy-Item "Microsoft.PowerShell.Core\Registry::$sourceKey" -Destination $parentDestination <#-Recurse -force
			}
		}
	}#>
}

function initializeSRP(){
	$caption=(Get-WmiObject win32_operatingsystem).caption
  if ( $caption.EndsWith("Pro") ) {
    $winPro=$true
  }

  $wow64SaferPath="HKLM:\SOFTWARE\WOW6432Node\Policies\Microsoft\Windows\Safer"
  $sid = (Get-WmiObject win32_useraccount -Filter "name = '${env:username}'").sid
  $sidSaferPath = "Microsoft.PowerShell.Core\Registry::HKEY_USERS\$sid\Software\Microsoft\Windows\CurrentVersion\Group Policy Objects"

	$member=$false
	if (test-path $leveKeyPath){
		$properties = Get-ItemProperty -Path $leveKeyPath
        if ($properties -ne $NULL){
		    $member = Get-Member -InputObject $properties -Name $leveKey
        }
	}
  if ( ($winPro -eq $true) -and (!$member) ){
    Write-Host -foregroundColor yellow "Please initialize (and restrict!) manually in order to use Policy editor."
    Read-Host -Prompt 'Press Enter to launch Local Security Policy management'
    start-process secpol.msc -wait
    Read-Host "Press enter to initialize SRP..."
  }


	initializeHKLMSrp
	Write-Host -ForegroundColor yellow "Automatically search and add paths to deny?"
	Write-Host "This may take a while..."
	$res=read-host
	if ($res -like 'Y'){
		findPathToDeny
	}
	Write-Host "SRP initiaized."
	if ($winPro -eq $true){
		checkSrpKeys
	}
	unlockSRP

}

# create srp registry
function initializeHKLMSrp{
  # "wsh", "wsf", "WSC", "vbs", "vbe", "VB", "URL", "SHS", "shb", "sct", "SCR", "REG", "ps1", "PIF", "PCD", "otf", "OCX", "MST", "MSP", "MSI", "MSC", "MDE", "MDB", "jse", "js", "ISP", "INS", "INF", "HTA", "HLP", "EXE", "CRT", "CPL", "COM", "CMD", "CHM", "BAT", "BAS", "ADP", "ADE"
  New-Item -Path $leveKeyPath -Force | out-null
  New-ItemProperty -Path $leveKeyPath -Name $leveKey -PropertyType DWord -Value $allowLevelValue | out-null
  New-ItemProperty -Path $leveKeyPath -Name ExecutableTypes -PropertyType MultiString -Value "wsh", "wsf", "WSC", "vbs", "vbe", "VB", "URL", "SHS", "shb", "sct", "SCR", "REG", "ps1", "PIF", "PCD", "otf", "OCX", "MST", "MSP", "MSI", "MSC", "MDE", "MDB", "jse", "js", "ISP", "INS", "INF", "HTA", "HLP", "EXE", "CRT", "CPL", "COM", "CMD", "CHM", "BAT", "BAS", "ADP", "ADE" | out-null
  New-ItemProperty -Path $leveKeyPath -Name PolicyScope -PropertyType DWord -Value 0 | out-null
  New-ItemProperty -Path $leveKeyPath -Name TransparentEnabled -PropertyType DWord -Value 1 | out-null

	$allowedList = gc $allowedConfFile

	foreach ($allowedPath in $allowedList){
		if (! ($allowedPath.startswith("%"))) {
			$allowedPath="$env:systemdrive\$allowedPath"
		}
		#$allowedPath = (gi $allowedPath).fullname
		$guid=[guid]::NewGuid()
		$guidKeyPath="$allowedPathParentKey\{$guid}"
		addAllowedPathRule $allowedPath $guidKeyPath
	}


}#initializeHKLMSrp



# lock srp
function lockSRP(){
  Set-ItemProperty -Path $leveKeyPath -Name $leveKey -Type DWord -Value $denyLevelValue
	checkSrpKeys
}

# unlock srp
function unlockSRP(){
  Set-ItemProperty -Path $leveKeyPath -Name $leveKey -Type DWord -Value $allowLevelValue
	checkSrpKeys
}

# add allowed path rule if not present
function addAllowedPathRule($path, $key){
	$already=$false
	if (test-path $allowedPathParentKey){
		while($already -ne $true){
			foreach ($allowedPath in (gci $allowedPathParentKey).pspath){
				$allowedPath = (Get-ItemPropertyValue $allowedPath -Name ItemData).trim('\')
				$path = $path.trim('\')
				if ($allowedPath -like $path){
					$already=$true
				}
			}
			break
		}
	} else {
		New-Item $allowedPathParentKey -Force | Out-Null
	}
	if ($already -ne $true){
	  New-Item -Path $key | Out-Null
		$currFileTime=(get-date).ToFileTime()
	  $currFileTimeHex='{0:x}' -f $currFileTime
		New-ItemProperty -Path $key -Name LastModified -PropertyType qword -value $currFileTime -Force | out-null
	  New-ItemProperty -Path $key -Name Description -PropertyType string -Force | out-null
		New-ItemProperty -Path $key -Name SaferFlags -PropertyType DWord -Value 0 -Force | out-null
	  New-ItemProperty -Path $key -Name ItemData -PropertyType string -value $path -Force | out-null
		#Add-Content $allowedConfFile $path
	}
}

# add deny path rule if not present
function addDeniedPathRule($path, $key){
	$already=$false
	if (test-path $deniedPathParentKey){
		while($already -ne $true){
			foreach ($deniedPath in (gci $deniedPathParentKey).pspath){
				$deniedPath = (Get-ItemPropertyValue $deniedPath -Name ItemData).trim('\')
				$path = $path.trim('\')
				if ($deniedPath -like $path){
					$already=$true
				}
			}
			break
		}
	} else {
		New-Item $deniedPathParentKey -Force | out-null
	}
	if ($already -ne $true){
		New-Item -Path $key | Out-Null
		$currFileTime=(get-date).ToFileTime()
		$currFileTimeHex='{0:x}' -f $currFileTime
		New-ItemProperty -Path $key -Name LastModified -PropertyType qword -value $currFileTime -Force | out-null
	  New-ItemProperty -Path $key -Name Description -PropertyType string -Force | out-null
	  New-ItemProperty -Path $key -Name SaferFlags -PropertyType DWord -Value 0 -Force | out-null
		New-ItemProperty -Path $key -Name ItemData -PropertyType string -value $path -Force | out-null
		#Add-Content $deniedConfFile $path
	}
}

#list current SRP rules
function listCurrentSrpRules{
	write-host
	#test exists
	$properties = Get-ItemProperty -Path $leveKeyPath
	$member = Get-Member -InputObject $properties -Name $leveKey
	if ($member){
	  $defaultLevelValue = Get-ItemPropertyValue -Path $leveKeyPath -name $leveKey
	  if ($defaultLevelValue -eq $denyLevelValue){
	    write-host "Defaul security level NOT ALLOWED"
	  } elseif ($defaultLevelValue -eq $allowLevelValue){
	    write-host "Defaul security level ALLOWED"
	  }

	<#
	$deniedPathParentKey="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers\0\Paths"
	$allowedPathParentKey="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers\262144\Paths"
	$allowedHashParentKey="HKLM:\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers\262144\Hashes"
	#>
	  if (Test-Path $deniedPathParentKey){
		write-host
	    write-host "Denied paths:"
	    $deniedPaths=(get-childitem $deniedPathParentKey | ForEach-Object {Get-ItemProperty $_.pspath})
	    foreach ($deniedPath in $deniedPaths)
	    {
	      write-host $deniedPath.ItemData
	    }
	  }

	  if (Test-Path $allowedPathParentKey){
	    write-host
	    write-host "Allowed paths:"
	    $allowedPaths=(get-childitem $allowedPathParentKey | ForEach-Object {Get-ItemProperty $_.pspath})
	    foreach ($allowedPath in $allowedPaths)
	    {
	      write-host $allowedPath.ItemData
	    }
	  }

	  if (Test-Path $allowedHashParentKey){
	    write-host
	    write-host "Allowed hash:"
	    $allowedHashes=(get-childitem $allowedHashParentKey | ForEach-Object {Get-ItemProperty $_.pspath})
	    foreach ($allowedHash in $allowedHashes)
	    {
	      write-host $allowedHash.friendlyname
	    }
	  }
	} else {
		write-host "SRP not set up..."
	}
}

#get all HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Group Policy Objects
function getAllGuiGPO(){
	$path = "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\Group Policy Objects\"
	$a = gci $path
	$guiGpoKeys=@()
	foreach ($s in $a) {
		if ($s.name.EndsWith("Machine") ){
			$guiGpoKeys+=$s.pspath+"\Software\Policies\Microsoft\Windows"
		}
	}
	return $guiGpoKeys
}

#get all SID GPO
function getAllSidGPO(){
	#New-PSDrive HKU Registry HKEY_USERS
	#Set-Location HKU:
	$sid = (Get-WmiObject win32_useraccount -Filter "name = '${env:username}'").sid
	#$a = gci  "HKU:\$sid\Software\Microsoft\Windows\CurrentVersion\Group Policy Objects\"
	$a = gci "Microsoft.PowerShell.Core\Registry::HKEY_USERS\$sid\Software\Microsoft\Windows\CurrentVersion\Group Policy Objects\"
	$sidGpoKeys=@()
	foreach ($s in $a) {
		if ($s.name.EndsWith("Machine") ){
			$sidGpoKeys+=$s.pspath+"\Software\Policies\Microsoft\Windows"
		}
	}
	return $sidGpoKeys
}

# findPathToDeny
function findPathToDeny(){
	Clear-Content "$OutputsFolder\accessChkRes.txt"
	$deniedConfFile="$ConfFolder\SRP\srpDeniedPaths.txt"
	$allowedConfFile="$ConfFolder\SRP\srpAllowedPaths.txt"
  $users  = @(
			"Users", "Everyone", "Authenticated Users", "Interactive"
		)
	$paths  = @(
			"${env:ProgramFiles}", "${env:ProgramFiles(x86)}", "${env:SYSTEMROOT}",
			"${env:ProgramData}\Microsoft\Windows Defender\Definition Updates"
		)

		if ([System.IntPtr]::Size -eq 4) {
			$accesChk = $UtilsFolderRelPath+"\accesschk.exe"
		} else {
			$accesChk = $UtilsFolderRelPath+"\accesschk64.exe"
		}
		foreach ($user in $users) {
			foreach ($path in $paths) {
				$accesChkParams = "-w -s -q -u -accepteula `"$user`" `"$path`""
				$command = "$accesChk $accesChkParams >> `"$OutputsFolder\accessChkRes.txt`""
				Write-Host "Searching writable paths in `"$path`" for `"$user`" user..."
				Invoke-Expression -Command:$command
			}
		}


		Write-Host -BackgroundColor black  -ForegroundColor green -NoNewline "Automatically add paths to blacklist? "
		$autoBlackList=read-host


		$SystemDrive = $env:SystemDrive
		$a = Get-Content $OutputsFolder\accessChkRes.txt
		$a = $a | Sort-Object

		#creating clean paths
		$aList=@()
		$cleanArray=@()
		foreach ($currS in $a)
		{
			$currSLength = $currS.Length
			if ($currSLength -gt 0) {
				$currPath=$currS.Substring(3, $currSLength-3)
				if ($currPath.StartsWith($SystemDrive)){
					$aList+=$currPath
				}
			}
		}
		$aList = $aList | Sort-Object
		for ($i=0; $i -le ($aList.Count); $i++){
			$currS = $aList[$i]
				if ($currS -ne $null) {
				#write-host "CURRS before: $currs"
				if ( ($currs -ne $NULL) -and (Test-Path $currS -PathType container) ){
					$currS+='\'
				} else {
					$currS = (Get-Item $currs).Directory.FullName+"\"
				}
				#write-host "CURRS after: $currs"
				#write-host ""
				#read-host
				$found=$false
				for ($j=0; $j -le ($cleanArray.Count); $j++){
					$nextS = $cleanArray[$j]
					if (($nextS -ne $NULL) -and (Test-Path $nextS -PathType container) ){
						$nextS+='\'
					}
					if ( (($currs -ne $NULL) -and ($nextS -ne $NULL)) -and  $nextS.StartsWith($currS) ){
						$found = $true
						break
					}
				}
				if ($found -eq $false){
					$cleanArray+=$currS
				}
			}
		}#creating clean paths

	#write-host "cleanArray: "
	#write-host $cleanArray
	#read-host

	$denieds=@()
	if (Test-Path "$systemSaferPath\CodeIdentifiers\0\Paths"){
		$deniedsKeys=(gci "$systemSaferPath\CodeIdentifiers\0\Paths")
		Foreach ($deniedsKey in $deniedsKeys)
		{
			$dK=Get-ItemPropertyValue "Microsoft.PowerShell.Core\Registry::$deniedsKey" -Name itemdata
			$denieds+=$dK
		}
	}

	foreach ($s in $cleanArray){
		if($s.length -gt 0){
			if ( ($autoBlackList -notlike 'Y') -or ($autoBlackList -notlike 'y') ){
				$thisPath=read-Host "Add $s ?"
			} else {
				Write-Host -ForegroundColor yellow $s
			}
			if ($thisPath -like 'Y' -OR $autoBlackList -like 'Y'){
        $guid=[guid]::NewGuid()
	      $guidKeyPath="$deniedPathParentKey\{$guid}"
	      #write-host $s $guidKeyPath
        #read-host

				if(! ($denieds.contains($s)) ){
					addDeniedPathRule $s $guidKeyPath
				}
				## append to file

				$dfC=(gc $deniedConfFile)
				if (! ($dfC -eq $NULL)){
					if (! $dfC.contains($s)){
						Add-Content $deniedConfFile $s
					}
				} else {
					Add-Content $deniedConfFile $s
				}
			}
		}
	}

}#findPathToDeny


function deleteAllSrp(){
	$guiGPOs = getAllGuiGPO
	Foreach ($guiGPO in $guiGPOs)
	{
		if (test-path "$guiGPO\Safer"){
			write-host -BackgroundColor red "Removing: $guiGPO\Safer"
			remove-item "$guiGPO\Safer"
		}
	}

	# User's SID keys
	$sidGPOs = getAllSidGPO
	Foreach ($sidGPO in $sidGPOs)
	{
		if (test-path "$sidGPO\Safer"){
			write-host -BackgroundColor red "Removing: $sidGPO\Safer"
			remove-item "$sidGPO\Safer"
		}
	}

}

function addCustomPaths(){
	$customDeniedPaths = Get-Content $ConfFolder\SRP\custom\srpDeniedPaths.txt
	$customAllowedPaths = Get-Content $ConfFolder\SRP\custom\srpAllowedPaths.txt

	foreach ($d in $customDeniedPaths){
		$guid=[guid]::NewGuid()
		$guidKeyPath="$deniedPathParentKey\{$guid}"
		addDeniedPathRule $d $guidKeyPath
	}
	echo $null > "$ConfFolder\SRP\custom\srpDeniedPaths.txt"

	foreach ($a in $customAllowedPaths){
		$guid=[guid]::NewGuid()
		$guidKeyPath="$allowedPathParentKey\{$guid}"
		addDeniedPathRule $a $guidKeyPath
	}
	echo $null > "$ConfFolder\SRP\custom\srpAllowedPaths.txt"

}
