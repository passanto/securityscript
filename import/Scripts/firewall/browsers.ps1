# outbound
New-NetFirewallRule -Direction Outbound -name "Google Chrome" -program "${env:programfiles(x86)}\Google\Chrome\Application\chrome.exe" -displayname "Allow Chrome" -Group "Google" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "Google Update" -program "${env:programfiles(x86)}\Google\Update\GoogleUpdate.exe" -displayname "Allow Google Update" -Group "Google" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "Firefox" -program "${env:programfiles}\Mozilla Firefox\firefox.exe" -displayname "Allow Firefox" -Group "Mozilla" -ErrorAction SilentlyContinue | out-null
