# outbound
New-NetFirewallRule -Direction Outbound -name "Skype" -program "${env:programfiles(x86)}\Skype\Phone\Skype.exe" -displayname "Allow Skype" -Group "Skype" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "Skype App" -program "${env:programfiles}\WindowsApps\Microsoft.SkypeApp_12.8.487.0_x64__kzf8qxf38zg5c\SkypeApp.exe" -displayname "Allow Skype App" -Group "Skype" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "SkypeHost" -program "${env:programfiles}\WindowsApps\Microsoft.SkypeApp_12.8.487.0_x64__kzf8qxf38zg5c\SkypeHost.exe" -displayname "Allow SkypeHost" -Group "Skype" -ErrorAction SilentlyContinue | out-null
