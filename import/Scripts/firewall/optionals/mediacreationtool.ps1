# outbound
New-NetFirewallRule -Direction Outbound -name "MediaCreationTool" -program "${env:programfiles}\Portables\MediaCreationTool\MediaCreationTool.exe" -displayname "Allow MediaCreationTool" -Group "MediaCreationTool" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "MediaCreationTool DiagTrackRunner" -program "${env:SystemDrive}\`$Windows.~WS\Sources\DiagTrackRunner.exe" -displayname "Allow MediaCreationTool DiagTrackRunner" -Group "MediaCreationTool" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "MediaCreationTool Setup" -program "${env:SystemDrive}\`$Windows.~WS\Sources\SetupHost.exe" -displayname "Allow MediaCreationTool Setup" -Group "MediaCreationTool" -ErrorAction SilentlyContinue | out-null
