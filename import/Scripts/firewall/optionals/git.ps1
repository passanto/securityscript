# outbound
$gitPath="${env:programfiles(x86)}\git"
if (! (test-path $gitPath) ){
  $gitPath="${env:programfiles}\git"
  if (! (test-path $gitPath) ){
    read-host "Check configured Git firewall rules..."
    exit
  }
}
New-NetFirewallRule -Direction Outbound -name "Git" -program "$gitPath\Git\mingw32\bin\git.exe" -displayname "Allow Git" -Group "Git" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "Git Remote Https" -program "$gitPath\Git\mingw32\libexec\git-core\git-remote-https.exe" -displayname "Allow Git Remote Hhttps" -Group "Git" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "Git Bash" -program "$gitPath\Git\git-bash.exe" -displayname "Allow Git Bash" -Group "Git" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "Git Bash MinTTY" -program "$gitPath\Git\usr\bin\mintty.exe" -displayname "Allow GitBash MinTTY" -Group "Git" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "Git Cmd" -program "$gitPath\Git\git-cmd.exe" -displayname "Allow GitCmd" -Group "Git" -ErrorAction SilentlyContinue | out-null
New-NetFirewallRule -Direction Outbound -name "Git Gui" -program "$gitPath\C:\Program Files\Git\mingw64\bin\wish.exe" -displayname "Allow Git Gui" -Group "Git" -ErrorAction SilentlyContinue | out-null
Write-Host -ForegroundColor yellow "Imported $rName rules"
