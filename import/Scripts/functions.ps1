#update "process_file" property
function updateScriptIndex($newScriptNum){
	$newScriptNum=[int]$newScriptNum
	$newScriptNum=++$newScriptNum
	#update properties file.process_file
	$processFilePath=".\import\Conf\securityScript.properties"
	$processFileText = "process_file=$newScriptNum"
	$newProcessFile = Get-Content $processFilePath | ForEach-Object{
		$_ -replace "^process_file=.*$",$processFileText
	}
	$newProcessFile | Set-Content $processFilePath
}

#update property file
function updateScriptProperties($property, $value){
	$processFilePath=".\import\Conf\securityScript.properties"
	$value = "$property=$value"
	$newProcessFile = Get-Content $processFilePath | ForEach-Object{
		$_ -replace "^$property=.*$",$value
	}
	$newProcessFile | Set-Content $processFilePath
}

#run script file
function runScriptFile($secScriptFileName){
	if (test-path .\import\Scripts\actions\administrator\$secScriptFileName){
		$ConfirmPreference = 'None'
		cd $pwd\
		& "$ScriptsFolder\actions\administrator\$secScriptFileName"
	} else {
		write-host -ForegroundColor red "Something's wrong..."
	}
}

# log errors
function errorsLog($msg){
	$errorLogFile="$OutputsFolder\errors.txt"
  Out-File $errorLogFile -Append -inputobject $(Get-Date)
	$msg | Out-File $errorLogFile -Append
}

# log errors
function userErrorsLog($msg){
	$errorLogFile="${env:homedrive}\${env:homepath}\Desktop\securityScriptErrors.txt"
  Out-File $errorLogFile -Append -inputobject $(Get-Date)
	$msg | Out-File $errorLogFile -Append
}

# error management
function errorManagement($err, $func){
	Write-Warning $_
	errorsLog $_
	Write-Host -BackgroundColor RED -ForegroundColor Yellow "Something went wrong with $func"
	Write-Host -BackgroundColor Yellow -ForegroundColor Red "Continue? (Y/N): "
	$continue = Read-Host
	if ($continue -ne 'Y' -or $continue -ne 'y'){
			read-host "Press ENTER to exit..."
			exit
	}
}

# error management
function userErrorManagement($err, $func){
	Write-Warning $_
	userErrorsLog $_
	Write-Host -BackgroundColor RED -ForegroundColor Yellow "Something went wrong with $func"
	Write-Host "Error log file: ${env:homedrive}\${env:homepath}\Desktop\securityScriptErrors.txt"
	Write-Host -BackgroundColor Yellow -ForegroundColor Red "Continue? (Y/N): "
	$continue = Read-Host
	if ($continue -ne 'Y' -or $continue -ne 'y'){
			read-host "Press ENTER to exit..."
			exit
	}
}

#check if autorun task exists
function checkAutorunTask(){
	$task=get-scheduledtask -taskname "Security Script Auto Startup" -ErrorAction silentlycontinue
	if ($task -eq $null){
		return $false
	} else {
		return $true
	}
}


#create autorun link
function createAutorunTask(){
	#register
	$runFile=get-item "$pwd\run.bat"
	$runFilePath=$runFile.fullname
	$runUser=whoami
	#Create a new trigger that is configured to trigger at startup
	$STTrigger = New-ScheduledTaskTrigger -AtLogOn -User $runUser
	#Name for the scheduled task
	$STName = "Security Script Auto Startup"
	#Action to run as
	$STAction = New-ScheduledTaskAction -Execute $runFilePath
	#Configure when to stop the task and how long it can run for. In this example it does not stop on idle and uses the maximum possible duration by setting a timelimit of 0
	$STSettings = New-ScheduledTaskSettingsSet -DontStopOnIdleEnd -ExecutionTimeLimit ([TimeSpan]::Zero) -DontStopIfGoingOnBatteries -AllowStartIfOnBatteries
	#Configure the principal to use for the scheduled task and the level to run as
	$STPrincipal = New-ScheduledTaskPrincipal -GroupId "BUILTIN\Administrators" -RunLevel "Highest"
	#Register the new scheduled task
	Register-ScheduledTask $STName -Action $STAction -Trigger $STTrigger -Principal $STPrincipal -Settings $STSettings | out-null
}

#create User autorun link
function createUserAutorunTask($runUser){
	#register
	$runFile=get-item "$pwd\run.bat"
	$runFilePath=$runFile.fullname
	#Create a new trigger that is configured to trigger at startup
	$STTrigger = New-ScheduledTaskTrigger -AtLogOn -User $runUser
	#Name for the scheduled task
	$STName = "Security Script $runUser Auto Startup"
	#Action to run as
	$STAction = New-ScheduledTaskAction -Execute $runFilePath
	#Configure when to stop the task and how long it can run for. In this example it does not stop on idle and uses the maximum possible duration by setting a timelimit of 0
	$STSettings = New-ScheduledTaskSettingsSet -DontStopOnIdleEnd -ExecutionTimeLimit ([TimeSpan]::Zero) -DontStopIfGoingOnBatteries -AllowStartIfOnBatteries
	#Configure the principal to use for the scheduled task and the level to run as
	$STPrincipal = New-ScheduledTaskPrincipal -UserId "$($env:USERDOMAIN)\$runUser"# -LogonType ServiceAccount
	#Register the new scheduled task
	Register-ScheduledTask $STName -Action $STAction -Trigger $STTrigger -Principal $STPrincipal -Settings $STSettings | out-null
}

#remove autorun link
function removeAutorunTask(){
	#unregister
	$STName = "Security Script Auto Startup"
	Unregister-ScheduledTask -TaskName $STName -Confirm:$false
}

#remove autorun link
function removeUserAutorunTask($runUser){
	#unregister
	$STName = "Security Script $runUser Auto Startup"
	Unregister-ScheduledTask -TaskName $STName -Confirm:$false
}

# WriteHostIfEnabled Function
function WriteHostIfEnabled{
    param (
     [parameter(Mandatory=$true)]
     [ValidateNotNullOrEmpty()]$Msg
    )
    if ($isWriteHostIfEnabled){
        Write-Host $Msg
    }
}

# test registry keu value
function Test-RegistryValue {
	param (
		[parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]$Path,
		[parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]$Value
	)
	try {
		Get-ItemProperty -Path $Path | Select-Object -ExpandProperty $Value -ErrorAction Stop | Out-Null
		return $true
	} catch {
		return $false
	}
}

# windows license check
function Get-ActivationStatus {
[CmdletBinding()]
    param(
        [Parameter(ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
        [string]$DNSHostName = $Env:COMPUTERNAME
    )
    process {
        try {
            $wpa = Get-WmiObject SoftwareLicensingProduct -ComputerName $DNSHostName `
            -Filter "ApplicationID = '55c92734-d682-4d71-983e-d6ec3f16059f'" `
            -Property LicenseStatus -ErrorAction Stop
        } catch {
            $status = New-Object ComponentModel.Win32Exception ($_.Exception.ErrorCode)
            $wpa = $null
        }
        $out = New-Object psobject -Property @{
            ComputerName = $DNSHostName;
            Status = [string]::Empty;
        }
        if ($wpa) {
            :outer foreach($item in $wpa) {
                switch ($item.LicenseStatus) {
                    0 {$out.Status = "Unlicensed"}
                    1 {$out.Status = "Licensed"; break outer}
                    2 {$out.Status = "Out-Of-Box Grace Period"; break outer}
                    3 {$out.Status = "Out-Of-Tolerance Grace Period"; break outer}
                    4 {$out.Status = "Non-Genuine Grace Period"; break outer}
                    5 {$out.Status = "Notification"; break outer}
                    6 {$out.Status = "Extended Grace"; break outer}
                    default {$out.Status = "Unknown value"}
                }
            }
        } else {$out.Status = $status.Message}
        $out
    }
}

function Takeown-File($path) {
	takeown.exe /A /F $path | out-null
	$acl = Get-Acl $path

	# get administraor group
	$admins = New-Object System.Security.Principal.SecurityIdentifier("S-1-5-32-544")
	$admins = $admins.Translate([System.Security.Principal.NTAccount])

	# add NT Authority\SYSTEM
	$rule = New-Object System.Security.AccessControl.FileSystemAccessRule($admins, "FullControl", "None", "None", "Allow")
	$acl.AddAccessRule($rule)

	Set-Acl -Path $path -AclObject $acl | out-null
}

function Takeown-Folder($path) {
	Takeown-File $path
	foreach ($item in Get-ChildItem $path) {
		if (Test-Path $item -PathType Container) {
			Takeown-Folder $item.FullName
		} else {
			Takeown-File $item.FullName
		}
	}
}


function Test-PendingReboot
{
	$result = @{
		CBSRebootPending =$false
		WindowsUpdateRebootRequired = $false
		FileRenamePending = $false
		SCCMRebootPending = $false
	}

	#Check CBS Registry
	$key = Get-ChildItem "HKLM:Software\Microsoft\Windows\CurrentVersion\Component Based Servicing\RebootPending" -ErrorAction Ignore
	if ($key -ne $null)
	{
			$result.CBSRebootPending = $true
	}

	#Check Windows Update
	$key = Get-Item "HKLM:SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\RebootRequired" -ErrorAction Ignore
	if($key -ne $null)
	{
			$result.WindowsUpdateRebootRequired = $true
	}

	#Check PendingFileRenameOperations
	$prop = Get-ItemProperty "HKLM:SYSTEM\CurrentControlSet\Control\Session Manager" -Name PendingFileRenameOperations -ErrorAction Ignore
	if($prop -ne $null)
	{
			#PendingFileRenameOperations is not *must* to reboot?
			#$result.FileRenamePending = $true
	}

	#Check SCCM Client <http://gallery.technet.microsoft.com/scriptcenter/Get-PendingReboot-Query-bdb79542/view/Discussions#content>
	try
	{
			$util = [wmiclass]"\\.\root\ccm\clientsdk:CCM_ClientUtilities"
			$status = $util.DetermineIfRebootPending()
			if(($status -ne $null) -and $status.RebootPending){
					$result.SCCMRebootPending = $true
			}
	}catch{}

	#Return Reboot required
	return $result.ContainsValue($true)
}
