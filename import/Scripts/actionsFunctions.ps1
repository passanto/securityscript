function changeUserPassword($user){
  # current user
  #(Get-WMIObject -class Win32_ComputerSystem).username
  #
	$pwdsOK = $false;
	$uName=$user.name
	$changeAdminPwd = Read-Host "Change $uName's password?"
	if ($changeAdminPwd -like 'Y'){
		while ($pwdsOK -ne $true){
			$password = Read-Host "Enter new $uName's password" -AsSecureString
			$confirmpassword = Read-Host "Confirm new $uNameuName's password" -AsSecureString
			$pwd1_text = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($password))
			$pwd2_text = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($confirmpassword))
			if($pwd1_text -ne $pwd2_text) {
			   Write-host -BackgroundColor red "Entered passwords are not same."
			} else {
				$userslist  = get-wmiobject win32_useraccount
				foreach($userIndex in $userslist){
          #Write-host "userindex.caption:"$userIndex.caption
					if ($userIndex.caption -eq $user.caption){
						$usercaption = $userIndex.caption.replace("\","/")
						try{
              #Write-host "intry"
							([adsi]"WinNT://$usercaption").setpassword($password)
							$pwdsOK = $TRUE
							Write-Host "$uName's password changed"
              break
						} catch {
							Write-Host "Password invalid"
						}
					}
				}
			}
		}
	}
}


function checkPassword(){
	$pass = read-host -prompt "Enter a password for the user"
	$passLength = 14
	if ($pass.Length -gt 0){
		if ($pass.Length -ge $passLength){
			$pw2test = $pass
			$isGood = 0
			If ($pw2test -match "[^a-zA-Z0-9]") {
				#check for special chars
				#write-host "yes special"
				$isGood++
			}
			If ($pw2test -match "[0-9]") {
				#write-host "yes 0-9"
				$isGood++
			}
			If ($pw2test -cmatch "[a-z]")	{
				#write-host "yes a-z"
				$isGood++
			}
			If ($pw2test -cmatch "[A-Z]")	{
				#write-host "yes A-Z"
				$isGood++
			}
			If ($isGood -ge 3)	{
				$pw2test + " is a good password" | Out-Default
	      return $pw2test
			} else{
				$pw2test + " is not a good password" | Out-Default
	      checkPassword
			}
		} else {
			write-host "$pass is not long enough - Passwords must be at least $passLength characters long"
			checkPassword
		}
	} else {
		write-host "Passwords cannot be empty"
		checkPassword
	}
}
