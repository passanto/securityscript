###################
# BEGIN
###################


<#
Warn message
#>
#Write-Host -ForegroundColor Red "Enable applications whitelisting lockdown!"
$Admin=(([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
if ($Admin){
	Write-Host -ForegroundColor red "Switch to a Standard user account."
	read-host "Press enter to exit"
	return
}
$testNet = $true
try{
	$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationLevel quiet
} catch {
	Write-Host -ForegroundColor Yellow "The system should be disconnected."
}
while ($testNet -eq $false)
{
	Get-NetAdapter | Enable-Netadapter -Confirm:$false -ErrorAction SilentlyContinue | out-null
	#echo "testing again connection"
	try{
		$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationLevel quiet
	} catch {
		# offline
	}
}
Write-Host
Write-Host -ForegroundColor Yellow "Make sure that the system is online."
Write-Host -ForegroundColor Red "Do not browse the internet"


<#
Windows Update | check logic
#>
try{
	Write-Host
	Read-Host -Prompt "Press Enter to open Windows Update..."
	start-process ms-settings:windowsupdate
	$id=(get-process SystemSettings).id
	Wait-Process -Id $id
} catch {
	errorManagement $_ "Updating Windows"
}

if (Test-PendingReboot){
	Write-Host "A reboot is required."
	$reboot=read-Host "Restart the process?"
	if (($reboot -like 'Y')){
		# restart
		write-Host "Press enter to reset the process and reboot"
		read-Host "Login as Administrator!"

		#reset script props file
		$DefaultConfFolder=(get-item -path "import\Conf\default").fullname
		copy-item -path "$DefaultConfFolder\securityScript.properties" -destination $ConfFolder

		# remove users tasks
		$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
		$users=$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
			$groups = $_.Groups() | Foreach-Object {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)}
			$_ | Select-Object @{n='UserName';e={$_.Name}},@{n='Groups';e={$groups -join ';'}}
		}
		foreach ($user in $users){
			if ($user.Groups -eq 'Users'){
				if ($user.Groups -eq 'Users'){
					removeUserAutorunTask $user.username
				}
			}
		}
		Restart-Computer -force
	} else {
		#no restart
		read-Host "Press enter to reboot and Login as Administrator!"
		Restart-Computer -force
	}
} else {
	# no reboot required
	read-Host "Press Enter to exit and Login with the Administrator account."
	logoff
}
