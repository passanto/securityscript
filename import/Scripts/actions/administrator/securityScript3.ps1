#Requires -RunAsAdministrator

###################
# BEGIN
###################


<#
firewall settings
#>
try{
    Write-Host
    # enable firewall for all the profiles (priv, pub, domain)
    Set-NetFirewallProfile -All -Enabled  true
    # blocl outbound and inbound connection by default
    Set-NetFirewallProfile -All -DefaultInboundAction Block -DefaultOutboundAction Block
    # notification settings
    Set-NetFirewallProfile -All -AllowUnicastResponseToMulticast false -NotifyOnListen true
    # log file size setting
    Set-NetFirewallProfile -All -LogMaxSizeKilobytes 32767
    # successful and failed connection logging settings
    Set-NetFirewallProfile -All -LogAllowed false -LogBlocked true
    write-host -ForegroundColor Yellow "Windows Firewall settings applied"
} catch {
  errorManagement $_ "firewall settings"
}


<#
disabling or removing firewall rules
#>
try{
  Write-Host
  $exportFwRule = Read-Host -Prompt 'Export current firewall rules?'
  if ($exportFwRule -like 'Y'){
    $expFile="$OutputsFolder\baselines\firewall\00__InitialFirewallRules.wfw"
    if (test-path $expFile){
      remove-item $expFile
    }
    netsh advfirewall export "$OutputsFolder\baselines\firewall\00__InitialFirewallRules.wfw"  | out-null
  }
  Write-Host -ForegroundColor Green 'Delete all firewall rules? Y/N'
  $deleteFwRule = Read-Host -Prompt 'it is strongly suggested to delete'
  $fwRules=Get-NetFirewallRule
  if ($deleteFwRule -like 'Y'){
    Remove-NetFirewallRule
    write-host -ForegroundColor Yellow "Deleted all Windows Firewall rules"
  } else {
    Write-Host -ForegroundColor Green 'Disable all firewall rules? Y/N'
    Write-Host -ForegroundColor Red 'disabling is the minimal suggested option' -NoNewline
    $disableFwRule = Read-Host -Prompt ' '
    if ($disableFwRule -like 'Y'){
      foreach ($fwRule in $fwRules)
      {
          Disable-NetFirewallRule -Name  $fwRule.Name
      }
      write-host -ForegroundColor Yellow "Disabled all Windows Firewall rules"
    }
  }
} catch {
  errorManagement $_ "firewall rules removal"
}


<#
importing firewall rules
#>
try{
  Write-Host
  importMinimalAndBrowsersRules
  Write-Host -ForegroundColor yellow "Imported minimal + browsers firewall rules"

  importOptionalsRules

	$open=Read-Host 'Open Windows Advanced Firewall?'
    if ($open -like 'Y'){
      & wf.msc
    }
    read-Host 'Press Enter to continue the process...'
} catch {
  errorManagement $_ "firewall rules addition"
}


<#
disable windows store
#>
try{
  Write-Host
    $disableWindowsStore = Read-Host -Prompt 'Disable Windows Store? (Y/N)'
    if ($disableWindowsStore -like 'Y'){
        $path="HKLM:\SOFTWARE\Policies\Microsoft"
        $winStorePath="\WindowsStore"
        $winStorePathKey="WindowsStore"
        $winStoreKey="RemoveWindowsStore"
        $winStoreValue=1
        Get-ItemProperty -Path $micrososftKeyPath$winStorePath -ErrorVariable winStoreKeyError -ErrorAction Continue | out-null
        if ($winStoreKeyError -ne ''){
            New-Item -Path $micrososftKeyPath -Name $winStorePathKey -Force | out-null
            New-ItemProperty -Path $micrososftKeyPath$winStorePath -Name $winStoreKey -Value $winStoreValue -PropertyType DWORD -ErrorAction Continue | out-null
        } else {
            Set-ItemProperty -Path $micrososftKeyPath$winStorePath -Name $winStoreKey -Value $winStoreValue -ErrorAction Continue | out-null
        }
        Write-Host -ForegroundColor Yellow  "Disabled Windows Store"
    }
} catch {
  errorManagement $_ "disable windows store"
}


<#
SRP
#>
try {
  Write-Host
  Write-Host "Software restriction policy:"
  initializeSRP
  checkSrpKeys
} catch {
  errorManagement $_ "Application whitelisting"
}


<#
Services
SDRSVC - Windows backup - check if disableable
#>
try {
  Write-Host
	## export current services status
	$exportPath = $OutputsFolder+'\ServicesBaseLine.csv'
	Write-Host -ForegroundColor red "Before setting the Services, an export will be created in the $exportPath file"
	Get-CimInstance win32_service | select name, caption, state, startmode  | export-csv -encoding UTF8 -notype -path $exportPath
	Write-Host -ForegroundColor yellow "The file can be viewed with any Excel compatible program"

  $list = @()
  [System.Collections.ArrayList]$services = $list
  Get-Content "$ConfFolder\services2disable.txt" | ForEach-Object{
    $services.Add($_) | out-null
	}
  forEach ($service in $services)
  {
      if ($service -eq 'seclogon' -or $service -eq 'LanmanWorkstation'){
        Write-Host
          if ($service -eq 'seclogon'){
          #check seclogon
              $seclogonDispName=(get-service -Name $service).DisplayName
              Write-Host -ForegroundColor Red "'$seclogonDispName' service is required for EMET"
              Write-Host -ForegroundColor Red "If disabled the 'run as administrator' feature will not be available"
              Write-Host -ForegroundColor Red -NoNewline "Disable '$seclogonDispName' service ? "
              $deleteSeclogon = Read-Host
              if ($deleteSeclogon -ne 'Y' -or $deleteSeclogon -ne 'y'){
                  Write-Host -ForegroundColor Yellow "'$seclogonDispName' will not be disabled"
              } else {
                  Stop-Service $service -ErrorAction Continue | Out-Null
                  Set-Service $service -StartupType Disabled -ErrorAction Continue | Out-Null
                  Write-Host -ForegroundColor Yellow "'$seclogonDispName' is disabled - consider an alternative to EMET"
              }
           } elseif ($service -eq 'LanmanWorkstation') {
           #check LanmanWorkstation
              $lanmanWorkstationDispName=(get-service -Name $service).DisplayName
              Write-Host -ForegroundColor Red "'$lanmanWorkstationDispName' service is required for creation of Windows Image"
              Write-Host -ForegroundColor Red -NoNewline "Disable '$lanmanWorkstationDispName' service ? "
              $deleteLanmanW = Read-Host
              if ($deleteLanmanW -ne 'Y' -or $deleteLanmanW -ne 'y'){
                  Write-Host -ForegroundColor Yellow "'$lanmanWorkstationDispName' will not be disabled"
              } else {
                  Stop-Service $service -ErrorAction Continue | Out-Null
                  Set-Service $service -StartupType Disabled -ErrorAction Continue | Out-Null
                  Write-Host -ForegroundColor Yellow "$lanmanWorkstationDispName is disabled"
              }
           }
       } else {
          Stop-Service $service -ErrorAction ignore | Out-Null
          Set-Service $service -StartupType Disabled -ErrorAction ignore | Out-Null
      }
  }
  Write-Host -ForegroundColor Yellow "Services disabled"
  $disabledExportPath = $OutputsFolder+'\ServicesDisabled.csv'
  Write-Host -ForegroundColor red "An export has been created in the $disabledExportPath file"
  Get-CimInstance win32_service | select name, caption, state, startmode  | export-csv -encoding UTF8 -notype -path $disabledExportPath
} catch {
  errorManagement $_ "Services disabling"
}


<#
Disable seeding of updates to other computers via Group Policies
No script available till now
#>
try {
  Write-Host
  force-mkdir "HKLM:\SOFTWARE\Policies\Microsoft\Windows\DeliveryOptimization"
  Set-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows\DeliveryOptimization" "DODownloadMode" 0
} catch {
  errorManagement $_ "Disable seeding of updates"
}


<#
Disable DCOM
No script available till now
#>


<#
Stop Logins from the Network
No script available till now
Use secedit with template
#>
try {
  Write-Host
	#Write-Host -ForegroundColor Red "Will Simple Software Restriction Policy be installed? (Y/N)"
	#Write-Host -NoNewline "If this Windows version support the default, then suggested NO."
	#$ssrpRequired = Read-Host
  $caption=(Get-WmiObject win32_operatingsystem).caption
  if ( $caption.EndsWith("Pro") ) {
    $winPro=$true
  }
	if (! ($winPro -eq $true) ){#($ssrpRequired -like 'Y'){
        Write-Host "Looks like SSRP has been installed... NetLogin will behave accordingly."
        $infFile = "$ConfFolder\net_login_ssrp.inf"
    } else {
        $infFile = "$ConfFolder\net_login_full.inf"
    }
    if (Test-Path $infFile){
	    secedit /configure /db "$OutputsFolder\net_login.sdb" /cfg $infFile | Out-Null
        Write-Host  -ForegroundColor Yellow "Login from network restricted"
    } else {
        Write-Host -ForegroundColor Red !$infFile missing!
    }
} catch {
  errorManagement $_ "Disable Logins from the Network"
}


<#
Anti Exploit
EMET | Malware Bytes
#>
try{
  Write-Host
  $winVer = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion' -Name ReleaseID | Select-Object ReleaseID).ReleaseId
  if ($winVer -lt "1709"){
    Write-Host "This Windows version needs an Anti-Exploit solution"
  	$secLogonServ=get-service -Name "seclogon"
  	$secLogonStatus=$secLogonServ.StartType
  	if ($secLogonStatus -eq "Disabled"){
  		Write-Host -ForegroundColor Red "Service '$secLogonServ.DisplayName' is $secLogonStatus"
  		Write-Host -ForegroundColor Red -NoNewline "Enable it in order to install EMET? (Y/N)"
  		$secLogonEnabling = Read-Host
  		if ($secLogonEnabling -like "Y"){
  			Set-Service seclogon -startuptype "manual"
  		} else {
  			Write-Host -ForegroundColor Red "Install Malwarebytes Anti-Exploit? (Y/N)"
  			Write-Host -ForegroundColor yellow -nonewline "If not installed EMET, suggested Yes:"
  			$mbaeInstalling = Read-Host
  			if ($mbaeInstalling -like 'Y'){
          $installers =  gci $ThirdPartyInstallerFolder
          foreach ($inst in $installers){
            if ($inst.tostring().startsWith("mbae")){
      				# MBAE INSTALL
  				    msiexec.exe /i $inst.fullname /qn /passive | out-null
              $mbaInstalled = $true
      			}
          }
          if (!$mbaInstalled) {
    				Write-Host -ForegroundColor Red "Malwarebytes Anti-Exploit installer not found"
            errorManagement $_ "Malwarebytes Anti-Exploit installation"
    			}
        }
      }
    }#secLogonStatus -eq "Disabled
  	if ($secLogonStatus -ne "Disabled" -or $secLogonEnabling -like "Y"){
  		# EMET INSTALL
  		$installers =  gci $ThirdPartyInstallerFolder
      foreach ($inst in $installers){
        if ($inst.tostring().startsWith("EMET")){
          msiexec.exe /i $inst.fullname /qn /passive | out-null
          $emeInstalled=$true
          #start /wait $EMETinstallerCommand
          #find emet installation folder:
          Write-Host -foregroundColor Red "Configure EMET..."
          <#$EMETFolder = gci ${env:ProgramFiles(x86)} -filter EMET* -ErrorAction SilentlyContinue
          if (!$EMETFolder){
              $EMETFolder = gci ${env:ProgramFiles} -filter EMET*
          }
          if ($EMETFolder){
            $EMETfullPath=$EMETFolder.fullname
            $importEmet="`"$EMETfullPath\EMET_Conf.exe`" --import `"$ConfFolder\EMET_Endpoint_Profile.xml`""
            Invoke-Expression -Command:$importEmet | Out-Null
            Write-Host -ForegroundColor Yellow "EMET installed"
            Write-Host -BackgroundColor Red "Reboot and continue..."
            $emeInstalled = $true
          } else {
            write-host "Error configuring EMET"
            read-Host "Press enter to continue"
          }#>
          break
        }
      }
      if (!$emeInstalled) {
        write-host "EMET installer not found"
      }
  	}
  }
} catch {
  errorManagement $_ "EMET installation"
}



#update properties file.process_file
updateScriptIndex($secPropsProcessFile)
createAutorunTask
write-host -ForegroundColor red "System needs to be restarted"
write-host -ForegroundColor yellow "The process will start automatically"
Write-Host
Read-Host "Press enter to reboot..."
Restart-Computer -force
