#Requires -RunAsAdministrator

###################
# BEGIN
###################


$Admin=(([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
if (!$Admin){
	Write-Host -ForegroundColor red "Switch to the Administrator User account."
	read-host "Press enter to exit"
	return
}


# remove users tasks
$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
$users=$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
	$groups = $_.Groups() | Foreach-Object {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)}
	$_ | Select-Object @{n='UserName';e={$_.Name}},@{n='Groups';e={$groups -join ';'}}
}
foreach ($user in $users){
	if ($user.Groups -eq 'Users'){
		if ($user.Groups -eq 'Users'){
			removeUserAutorunTask $user.username
		}
	}
}


function Test-RegistryValue {
	param (
	 	[parameter(Mandatory=$true)]
	 [ValidateNotNullOrEmpty()]$Path,
		[parameter(Mandatory=$true)]
	 [ValidateNotNullOrEmpty()]$Value
	)

	try {
		if (test-path $path){
			$ip=Get-ItemProperty -Path $Path | Select-Object -ExpandProperty $Value -ErrorAction Stop
		 	if ($ip){
				return $true
			}
		}
		return $false
	} catch {
		return $false
	}
}



<#
Disconnect from the network
#>
try{
	$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationLevel quiet
	while ($testNet -eq $true)
	{
		Write-Host -ForegroundColor Yellow "The system seems connected to internet"
		Write-Host -ForegroundColor Yellow "It is strongly reccomended to diconnect from the network"
		Write-Host -ForegroundColor Red "Continue with active connection? (Y/N)"
		$continueActive = Read-Host "Suggested No"
		if ($continueActive -like 'Y'){
			$continueActive = Read-Host "Continue with active connection? (Y/N)"
			if ($continueActive -like 'Y'){
				break
			}
		} else {
			Read-Host 'Press Enter to automatically disconnected all the network adapters'
			Get-NetAdapter | Disable-Netadapter -Confirm:$false -ErrorAction SilentlyContinue | out-null
			$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationLevel quiet
		}
	}
} catch {
	errorManagement $_ "Disconnect from the network after Update"
}


<#
additional software
#>
try{
	unlockSRP
	checkSrpKeys

	### CHROME
	Write-Host
	$install = Read-Host -Prompt "Install Google Chrome? "
	if ($install -like 'Y'){
		$chromeInstaller="$InstallersFolder\ChromeStandaloneSetup64.exe"
		$chromeParams =  "/silent /install"
		start-process $chromeInstaller $chromeParams -nonewwindow -wait
		Unregister-ScheduledTask -TaskName "GoogleUpdateTaskMachineCore" -Confirm:$false
		Unregister-ScheduledTask -TaskName "GoogleUpdateTaskMachineUA" -Confirm:$false
		remove-Item -path HKLM:\SYSTEM\CurrentControlSet\Services\gupdate
		remove-Item -path HKLM:\SYSTEM\CurrentControlSet\Services\gupdatem
		<#
			remove the Google Chrome Installer autorun
		#>
		#start remove chrmstp autorun
		$items=gci "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\"
		foreach ($item in $items){
			$RegLeaf = "(Default)"
			if (test-path $item.pspath){
				if (Test-RegistryValue $item.pspath $RegLeaf ){
					$gi=Get-ItemPropertyValue -path $item.pspath -name $RegLeaf -ErrorAction SilentlyContinue
					if ($gi -eq "Google Chrome"){
						remove-item $item.pspath
					}
				}
			}
		}
		$prefFound=$false
		while (!$prefFound){
			$prefFile="${env:programfiles(x86)}\Google\Chrome\Application\master_preferences"
			if (! (Test-Path $prefFile) ){
				write-host "Chrome pref file not found here: $prefFile"
				$res=read-host "Skip?"
				if ($res -like 'Y'){
					break
				}
				$prefFile="${env:programfiles}\Google\Chrome\Application\master_preferences"
			} else {
				$prefFound=$true
				Copy-Item "$ConfFolder\chrome.json" $prefFile
			}
		}
		Write-Host -foregroundcolor yellow "Google Chrome installed"
	}
} catch {
	errorManagement $_ "Google Chrome"
}


try{
		### FIREFOX
		Write-Host
		$install = Read-Host -Prompt "Install Mozilla Firefox? "
		if ($install -like 'Y'){
			$firefoxInstaller = (gci $InstallersFolder -filter "Firefox*" -FILE).fullname
			$firefoxIniFile="$ConfFolder\firefox.ini"
			$firefoxIni="/INI= $firefoxIniFile"
			$found=$false
			while(!$found){
				if ( (test-path $firefoxInstaller) -and (test-path $firefoxIniFile) ){
					$found=$true
					start-process $firefoxInstaller $firefoxIni -nonewwindow -wait
					Write-Host -foregroundcolor yellow "Mozilla Firefox installed"
					if ([System.IntPtr]::Size -eq 4) {
						$psexec="$UtilsFolderRelPath\PsExec.exe"
					}else{
						$psexec="$UtilsFolderRelPath\PsExec64.exe"
					}

					$ffEXE="$env:programfiles\Mozilla Firefox\firefox.exe"
					if (! (test-path $ffEXE)){
						$ffEXE="${env:programfiles(x86)}\Mozilla Firefox\firefox.exe"
						if (! (test-path $ffEXE)){
							Write-Host "Unable to locate Firefox.exe in usual paths."
							$skip=read-host "Skip Firefox integrity fix?"
							$found = $false
							while ($found -eq $false){
								$ffEXE = read-host "Enter Firefox.exe full path:"
								if ( (Test-Path $ffEXE) -eq $true ){
									Write-Host "Firefox.exe is locate here: $ffEXE"
									$found=$true
								}
							}
						}
					}


					$appDataFFs=@()
					# ff first run simulation for admin user
					Start-Process -NoNewWindow $ffEXE
					$ffStop = $false
					while ($ffStop -eq $false){
						$ffProc = Get-Process Firefox -ErrorAction SilentlyContinue
						if ($ffProc -ne $NULL){
							$appDataFF="${env:USERPROFILE}\appdata"
							$local = test-path "$appDataFF\Local\Mozilla\"
							$roaming = test-path "$appDataFF\Roaming\Mozilla\"
							if ( ($local -eq $true) -and ($roaming -eq $true) ){
								#ff initialized
								$ffProc.CloseMainWindow() | out-null
								if (!$ffProc.HasExited) {
									$ffProc | Stop-Process -Force | out-null
									$ffStop=$true
									$appDataFFs+=$appDataFF
								}
							}
						} else {
							$ffStop=$true
						}
					}
					Write-Host -foregroundcolor yellow "Mozilla Firefox Administrator's folders initialized"


					# ff first run simulation for all users
					foreach ($user in $users){
						if ($user.Groups -eq 'Users'){
							$userName=$user.username
							$psExecuted=$false
							while(!$psExecuted){
								$passwd=read-host "Enter the $userName password"
								try{
									$command="$psexec -u $userName -p $passwd -d `"$ffEXE`" | out-null "
									Invoke-Expression -Command:$command 2>&1
									$psExecuted=$true
								} catch {
									write-host -BackgroundColor red "Something went wrong while initializing Firefox for $userName"
									$res= read-host "Try with a different password?"
									if (!($res -like 'Y')){
										$psExecuted=$true
									}
								}
								$terminated=$false
								while(! ($terminated -eq $true)){
									$p=Get-Process "Firefox" -IncludeUserName
									if ($p -ne $null){
										if ($p.UserName.EndsWith($userName)){
											$appDataFF="$env:systemdrive\Users\$userName\AppData"
											$local = test-path "$appDataFF\Local\Mozilla\"
											$roaming = test-path "$appDataFF\Roaming\Mozilla\"
											if ( ($local -eq $true) -and ($roaming -eq $true) ){
												Write-Host -foregroundcolor yellow "Mozilla Firefox $userName's folders initialized"
												#ff initialized
												$p.CloseMainWindow() | out-null
												if (!$p.HasExited) {
													$p | Stop-Process -Force | out-null
													$terminated = $true
													$appDataFFs+=$appDataFF
												}
											}
										}
									}# get process
								}# while not terminated
							}# ps execution
						}# standard users
					}# loop users

					Foreach ($appDataFF in $appDataFFs)
					{
						Write-Host
						$path ="$appDataFF\Local\Temp\"
						& icacls $path /setintegritylevel`(oi`)`(ci`)  low /t | Out-Null

						$path ="$appDataFF\Local\Mozilla\"
						& icacls $path /setintegritylevel`(oi`)`(ci`) low /t | Out-Null

						$path ="$appDataFF\Roaming\Mozilla\"
						& icacls $path /setintegritylevel`(oi`)`(ci`) low /t | Out-Null

						$path=(gi "$appDataFF\..\Downloads").fullname
						Write-Host "Set $path folder to low integrity?"
						$reply=read-host "Suggested Y"
						if ($reply -NotLike 'y'){
							& icacls $path /setintegritylevel`(oi`)`(ci`) low /t | Out-Null
						}
						#Write-Host -foregroundcolor yellow "$appDataFF integrity levels configured"
					}# integrity level for all users
				} else {
					Write-Host "Unable to locate Firefox installers or conf file."
					$skip=read-host "Skip Firefox installation?"
					if (($skip -like 'Y')){
						$found=$true
					}
				} # find exe
			}# found installers
		}# find installers
} catch {
	errorManagement $_ "Install Mozilla Firefox"
}



try{
		### KEEPAS
		Write-Host
		$install = Read-Host -Prompt "Install KeePass? "
		if ($install -like 'Y'){
			$keepassInstaller = (gci $InstallersFolder -filter "KeePass*" -FILE).fullname
			$keepassParams="/VERYSILENT"
			start-process $keepassInstaller $keepassParams -nonewwindow -wait
			Write-Host -foregroundcolor yellow "KeePass installed"
			Write-Host -foregroundcolor yellow "It is suggested to update KeePass manually (as all the other softwares)"
		}
} catch {
	errorManagement $_ "Install KeePass"
}




try{
		### ACROBAT
		Write-Host
		$install = Read-Host -Prompt "Install Acrobat Reader DC? "
		if ($install -like 'Y'){
			$acrobatInstaller = (gci $InstallersFolder -filter "AcroRdrDC*" -FILE).fullname
			$acrobatParams="/sPB"
			start-process $acrobatInstaller $acrobatParams -nonewwindow -wait
			if (Get-ScheduledTask -TaskName "Adobe Acrobat Update Task" -ErrorAction SilentlyContinue){
				Unregister-ScheduledTask -TaskName "Adobe Acrobat Update Task" -Confirm:$false
			}
			if (test-path "HKLM:\SYSTEM\CurrentControlSet\Services\AdobeARMservice"){
				remove-Item -path "HKLM:\SYSTEM\CurrentControlSet\Services\AdobeARMservice"
			}
			<#
				remove the Adobe Type Manager autorun
			#>
			if (Test-RegistryValue "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Font Drivers\" "Adobe Type Manager"){
				Remove-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Font Drivers\" -name "Adobe Type Manager"
			}
			Write-Host -foregroundcolor yellow "Acrobat Reader DC installed"
		}
} catch {
	errorManagement $_ "Installing Acrobat Reader DC"
}





try{
		### SANDBOXIE
		Write-Host
		$install = Read-Host -Prompt "Install Sandboxie? "
		if ($install -like 'Y'){
			$numOK = $false
			while($numOK -eq $false){
				try {
					[int]$sandobxes = Read-host "How many sandboxes do you need? "
					if ($sandobxes -ge 1){
						$numOK = $true
					}
				} catch {
					$numOK = $false
				}
			}
			copy $ConfFolder\Sandboxie.ini ${env:systemroot}
			for ($i=0; $i -lt $sandobxes; $i++){
				$box=$i+1
				Add-Content ${env:systemroot}\Sandboxie.ini "[DefaultBox$box]`n" -Encoding Unicode
				get-content $ConfFolder\default_sandbox.ini | Add-Content ${env:systemroot}\Sandboxie.ini -Encoding Unicode
				Add-Content ${env:systemroot}\Sandboxie.ini "`n" -Encoding Unicode
			}

			$sandboxieInstaller = (gci $InstallersFolder -filter "Sandboxie*" -FILE).fullname
			$sandboxieParams = "/lang=1033 /install /S /D=${env:ProgramFiles}\Sandboxie"
			start-process $sandboxieInstaller $sandboxieParams -nonewwindow -wait
			Write-Host -foregroundcolor yellow "Sandboxie installed"
		}
} catch {
	errorManagement $_ "Installing Sandboxie"
}



<#
PSI
#>
try {
	Write-Host
	Write-Host -ForegroundColor Yellow "PSI will keep most of the software updated"
	Write-Host -ForegroundColor white "It can be configured as follow:"
	Write-Host -ForegroundColor white " -scan, warn, download and automatically install patches"
	Write-Host -ForegroundColor white " -scan, warn and download patches"
	Write-Host -ForegroundColor white " -scan and warn only"
	Write-Host -ForegroundColor Yellow "It is suggested to update manually (3dr option)"
	Write-Host
	$res=Read-Host  "Skip PSI installation?"
	if (! ($res -like 'Y') ){
		$psiInstallerFileName = (gci $InstallersFolder -filter "PSI*" -FILE).fullname
		$psiParameters = "/qn /passive"
		& $psiInstallerFileName $psiParameters | out-null
		#start-process $psiInstallerFileName $psiParameters -nonewwindow -wait
		Write-Host -ForegroundColor Yellow "PSI installed"
	}
} catch {
	errorManagement $_ "Installing PSI"
}


<#
Enabling secure logon
#>
try {
	Write-Host
	Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name "DisableCAD" -Type DWord -Value 0
 	Write-Host -ForegroundColor red "Secure Logon has been enabled"
	Write-Host -ForegroundColor Yellow "For login Users need to press Ctrl+Alt+Del and input their credentials"
} catch {
	errorManagement $_ "Enabling secure logon"
}


<#
Privacy
#>
try {
	Write-Host
 	Write-Host -ForegroundColor Yellow "If required, it is suggested to set Privacy configurations now"
	Write-Host -ForegroundColor Yellow "Start->Settings->Privacy"
	Read-Host -Prompt 'Press Enter to continue when done'
} catch {
	errorManagement $_ "Privacy"
}


<#
OneDrive
#>
try {
	Write-Host
 	Write-Host "OneDrive requires an additional whitelist rule, since its located in the user's folder"
	Write-Host -ForegroundColor Yellow 'Remove OneDrive? (Y/N):'
	$removeOneDrive = Read-Host -Prompt 'Suggested yes'
		if ($removeOneDrive -eq 'Y' -or $removeOneDrive -eq 'y'){
		## "Kill OneDrive process"
		$odProcess=get-process OneDrive* -ErrorAction silentlycontinue
		if ($odProcess -ne $null){
			$odProcess.Kill()
		}
		$explorerProcess=get-process explorer -ErrorAction silentlycontinue
		if ($explorer -ne $null){
			$explorer.Kill()
		}

		## "Remove OneDrive"
		if (Test-Path "$env:systemroot\System32\OneDriveSetup.exe") {
			& "$env:systemroot\System32\OneDriveSetup.exe" /uninstall
		}
		if (Test-Path "$env:systemroot\SysWOW64\OneDriveSetup.exe") {
			& "$env:systemroot\SysWOW64\OneDriveSetup.exe" /uninstall
		}

		## "Removing OneDrive leftovers"
		rm -Recurse -Force -ErrorAction SilentlyContinue "$env:localappdata\Microsoft\OneDrive"
		rm -Recurse -Force -ErrorAction SilentlyContinue "$env:programdata\Microsoft OneDrive"
		rm -Recurse -Force -ErrorAction SilentlyContinue "$env:systemdrive\OneDriveTemp"

		## "Disable OneDrive via Group Policies"
		$path="HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\OneDrive"
		if (!(Test-Path $path)) {
			#Write-Host "-- Creating full path to: " $path -ForegroundColor White -BackgroundColor DarkGreen
			New-Item -ItemType Directory -Force -Path $path | out-null
		}
		sp "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\OneDrive" "DisableFileSyncNGSC" 1

		## "Remove Onedrive from explorer sidebar"
		New-PSDrive -PSProvider "Registry" -Root "HKEY_CLASSES_ROOT" -Name "HKCR" | out-null
		mkdir -Force "HKCR:\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}"  | out-null
		sp "HKCR:\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" "System.IsPinnedToNameSpaceTree" 0 | out-null
		mkdir -Force "HKCR:\Wow6432Node\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}"  | out-null
		sp "HKCR:\Wow6432Node\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" "System.IsPinnedToNameSpaceTree" 0 | out-null
		Remove-PSDrive "HKCR" | out-null

		## "Removing run hook for new users"
		reg load "hku\Default" "$env:systemdrive\Users\Default\NTUSER.DAT" | out-null
		if (Test-Path "HKEY_USERS\Default\SOFTWARE\Microsoft\Windows\CurrentVersion\Run"){
			reg delete "HKEY_USERS\Default\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "OneDriveSetup" /f  | out-null
		}
		reg unload "hku\Default" | out-null

		# "Restarting explorer"
		$explorerProcess=get-process explorer -ErrorAction silentlycontinue
		while ($explorerProcess -eq $null){
			if ($explorerProcess -eq $null){
				start "explorer.exe"
				$explorerProcess=get-process explorer -ErrorAction silentlycontinue
			}
		}

		foreach ($item in (ls "$env:WinDir\WinSxS\*onedrive*")) {
			Takeown-Folder $item.FullName
			rm -Recurse -Force $item.FullName
		}
	}
} catch {
	errorManagement $_ "OneDrive admin removal"
}


<#
Enable DEP
#>
try {
	Write-Host
	$winDir = ${env:windir}
	& $winDir\system32\bcdedit /set nx AlwaysOn | out-null
 	Write-Host -ForegroundColor Yellow "DEP enabled"
} catch {
	errorManagement $_ "DEP"
}


<#
Disable dump file creation
#>
Try {
	Write-Host
	$x = Get-CimInstance Win32_OSRecoveryConfiguration -Property DebugInfoType
	if ($x.DebugInfoType -eq "0") {
		Write-Host -ForegroundColor Yellow "Dump file creation already disabled"
	} else {
		while ($x.DebugInfoType -ne "0"){
			$x.DebugInfoType = "0"
			Set-CimInstance -CimInstance $x -PassThru | out-null
			$x = Get-CimInstance Win32_OSRecoveryConfiguration -Property DebugInfoType
		}
		Write-Host -ForegroundColor Yellow "Dump file creation disabled"
	}
}
catch {
	errorManagement $_ "Disable dump file creation"
}


<#
Disallow remote assistance
#>
try {
		Write-Host
    $remoteAssistancePath="HKLM:\SYSTEM\CurrentControlSet\Control\Remote Assistance"
    $remoteAssistanceKey="fAllowToGetHelp"
    $remoteAssistanceKeyValue=0
    Set-ItemProperty -Path $remoteAssistancePath -Name $remoteAssistanceKey -Value $remoteAssistanceKeyValue -ErrorAction Continue
    Write-Host -ForegroundColor Yellow "Remote assistance disable"
} catch {
	errorManagement $_ "Disallow remote assistance"
}


<#
Restore Points
#>
try {
	Write-Host
	Enable-ComputerRestore -drive ${env:systemdrive}
	$size=Get-WMIObject Win32_Logicaldisk -filter "deviceid='${env:systemdrive}'" | Select @{Name="SizeGB";Expression={$_.Size/1GB -as [int]}}
	$size=$size.SizeGB
	$free=Get-WMIObject Win32_Logicaldisk -filter "deviceid='${env:systemdrive}'" | Select @{Name="FreeGB";Expression={[math]::Round($_.Freespace/1GB,2)}}
	$free=$free.FreeGB
	write-Host -foregroundcolor yellow "The system disk is $size GB"
	write-Host -foregroundcolor yellow "and has $free GB of space"
	$suggSpace = [int]($size / 10)
	$suggSpace = [String]$suggSpace+'GB'
	write-Host -foregroundcolor red "Allow $suggSpace of disk for restore points? (Y/N)"
	$allowSuggSpace = Read-Host -Prompt 'Suggested yes'
	if ($allowSuggSpace -eq 'Y' -OR $allowSuggSpace -eq 'y'){
		vssadmin Resize ShadowStorage /On=${env:systemdrive} /For=${env:systemdrive} /MaxSize=$suggSpace | out-null
	} else {
		write-Host -foregroundcolor red "Consider configuring available space for system restore"
	}
} catch {
	errorManagement $_ "Restore Points"
}


<#
16bit apps disabling
#>
try {
	Write-Host
	$16bitRegPath="HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppCompat"
	$16bitRegKey="VDMDisallowed"
	$16bitRegKeyValue=1

	Get-ItemProperty -Path $16bitRegPath$16bitRegKey -ErrorVariable 16bitError -ErrorAction silentlycontinue
	if ($16bitError -ne ''){
			New-Item -Path $16bitRegPath -Force | out-null
			New-ItemProperty -Path $16bitRegPath -Name $16bitRegKey -Value $16bitRegKeyValue -PropertyType DWORD -ErrorAction Continue | out-null
	} else {
			Set-ItemProperty -Path $16bitRegPath -Name $16bitRegKey -Value $16bitRegKeyValue -ErrorAction Continue | out-null
	}
	Write-Host -ForegroundColor Yellow  "Disabled 16 bit applications"
} catch {
	errorManagement $_ "16bit apps disabling"
}


<#
remote shell
#>
try {
	Write-Host
	$remoteShellRegPath="HKLM:\SOFTWARE\Policies\Microsoft\Windows\WinRM\Service\WinRS"
	$remoteShellRegKey="AllowRemoteShellAccess"
	$remoteShellRegKeyValue=0

	Get-ItemProperty -Path $remoteShellRegPath$remoteShellRegKey -ErrorVariable remoteShellError -ErrorAction silentlycontinue
	if ($remoteShellError -ne ''){
			New-Item -Path $remoteShellRegPath -Force | out-null
			New-ItemProperty -Path $remoteShellRegPath -Name $remoteShellRegKey -Value $remoteShellRegKeyValue -PropertyType DWORD -ErrorAction Continue | out-null
	} else {
			Set-ItemProperty -Path $remoteShellRegPath -Name $remoteShellRegKey -Value $remoteShellRegKeyValue -ErrorAction Continue | out-null
	}
	Write-Host -ForegroundColor Yellow  "Disabled Windows Remote Shell"
} catch {
	errorManagement $_ "Disabled Windows Remote Shell"
}


<#
pwd and lockout policies
#>
try {
	Write-Host
	Write-Host -NoNewline "Enforce passwords policies? "
	$pwdPolicies = Read-Host
	if ($pwdPolicies -like 'Y'){
		$infFile = "$ConfFolder\pwd_lockout.inf"
		$sdbFile="$OutputsFolder\pwd_lockout.sdb"
	  secedit /configure /db $sdbFile /cfg $infFile | Out-Null
	  Write-Host  -ForegroundColor Yellow "Password policies enabled."
	}
} catch {
	errorManagement $_ "Enforce passwords policies"
}


<#
administrator password
#>
# done in the per-user script


<#
BIOS password
#>
try {
	Write-Host
	Write-Host "It is almost mandatory to set a BIOS password, for physical security."
	Read-Host 'Press Enter to continue...'
} catch {
    Write-Warning $_
}


<#
drive encryption
#>
try {
	Write-Host
	Write-Host "Perform System drive encryption with Bitlocker (if available) or Veracrypt..."
	Read-Host 'Press Enter to continue...'
} catch {
	Write-Warning $_
}


<#
event log size
#>
try {
	Write-Host
	Limit-EventLog -LogName "Application" -MaximumSize 1000000KB
	Limit-EventLog -LogName "Security" -MaximumSize 1000000KB
	Limit-EventLog -LogName "System" -MaximumSize 1000000KB
	Write-Host -ForegroundColor yellow "Event logs configured"
} catch {
	errorManagement $_ "Event Log size"
}



<#
custom events
#>
try {
	Write-Host
	$customEvents = "$ConfFolder\custom_events\*"
	$eventsFolder = "${env:ProgramData}\Microsoft\Event Viewer\Views\"
	eventvwr.msc
	$p=Get-Process "mmc"
	while (!(test-path 	$eventsFolder)){}
	if ($p -ne $null){
		$p.Kill()
	}
	Copy-Item $customEvents $eventsFolder
	Write-Host -ForegroundColor yellow "Custom event logs imported"
} catch {
	errorManagement $_ "Event Log custom events"
}

#update properties file.process_file
updateScriptIndex($secPropsProcessFile)
createAutorunTask
Write-Host
write-host -ForegroundColor red "System needs to be restarted"
write-host -ForegroundColor yellow "The process will start automatically"
Write-Host
Read-Host "Press enter to reboot..."
Restart-Computer -force
