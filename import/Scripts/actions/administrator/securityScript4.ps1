#Requires -RunAsAdministrator

###################
# BEGIN
###################



<#
removes unwanted Apps
#>
try {
	Write-Host
	$res=read-host "Keep unwanted apps?"
	if (! ($res -eq 'Y')){
		Import-Module -DisableNameChecking $ScriptsFolder\lib\take-own.psm1
		Import-Module -DisableNameChecking $ScriptsFolder\lib\force-mkdir.psm1

		$apps=get-content "$ConfFolder\unwantedApps.txt"
		foreach ($app in $apps) {
			if (!$app.startsWith("#") -and (![string]::IsNullOrEmpty($app))){
				Get-AppxPackage -AllUsers $app | Remove-AppxPackage -ErrorAction SilentlyContinue
				Get-AppxPackage $app | Remove-AppxPackage -ErrorAction SilentlyContinue
			}
		}

		# Prevents "Suggested Applications" returning
		force-mkdir "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Cloud Content" | out-null
		Set-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Cloud Content" "DisableWindowsConsumerFeatures" 1

		Write-Host -ForegroundColor Yellow "Removed unwanted Apps"
	}
} catch {
	errorManagement $_ "Removing unwanted Apps"
}




<#
Disable live tiles
#>
try {
	Write-Host
	$res=read-host "Keep Live Tiles?"
	if (! ($res -eq 'Y')){
		# HKLM
		If (!(Test-Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications"))
		{
			New-Item -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" | Out-Null
		}
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" -Name "NoTileApplicationNotification" -Type DWord -Value 1
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" -Name "NoCloudApplicationNotification" -Type DWord -Value 1

		# Current user
		If (!(Test-Path "HKCU:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications"))
		{
			New-Item -Path "HKCU:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" | Out-Null
		}
		Set-ItemProperty -Path "HKCU:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" -Name "NoTileApplicationNotification" -Type DWord -Value 1
		Set-ItemProperty -Path "HKCU:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" -Name "NoCloudApplicationNotification" -Type DWord -Value 1

		# Standard user
		$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
		$users=$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
			$groups = $_.Groups() | Foreach-Object {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)}
			$_ | Select-Object @{n='UserName';e={$_.Name}},@{n='Groups';e={$groups -join ';'}}
		}
		foreach ($user in $users){
			if ($user.Groups -eq 'Users'){
				$uname=$user.UserName

				New-PSDrive -PSProvider Registry -Name $uname -Root HKEY_USERS | out-null
				$pnParent="Microsoft.PowerShell.Core\Registry::HKEY_USERS\$uname\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion"
				$pn="Microsoft.PowerShell.Core\Registry::HKEY_USERS\$uname\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications"
				reg load "HKU\$uname" "C:\Users\$uname\NTUSER.DAT" | out-null
				if (! (Test-Path $pn) ){
					new-item $pn -force | out-null
				}
				New-ItemProperty $pn -name NoTileApplicationNotification -PropertyType DWord -Value 1 -ErrorAction silentlycontinue | out-null
				New-ItemProperty $pn -name NoCloudApplicationNotification -PropertyType DWord -Value 1 -ErrorAction silentlycontinue | out-null
				[gc]::collect()
				start-sleep -s 3
				reg unload "HKU\$uname"  | out-null
				Remove-PSDrive $uname
			}
		}
		Write-Host -ForegroundColor Yellow "Live Tiles disabled for all users"
	}
} catch {
	errorManagement $_ "Disable live tiles"
}

Write-Host
Write-Host -ForegroundColor red "Switch to a Standard user account."
Read-Host -Prompt "Press Enter to continue"

$guid=[guid]::NewGuid()
$guidKeyPath="$allowedPathParentKey\{$guid}"
# NOTE REQUIRED - addAllowedPathRule $pwd.toString() $guidKeyPath
lockSRP
checkSrpKeys

# connecting
$testNet = $true
try{
	$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationLevel quiet
} catch {
	Write-Host -ForegroundColor Yellow "The system should be disconnected."
}
while ($testNet -eq $false)
{
	Get-NetAdapter | Enable-Netadapter -Confirm:$false -ErrorAction SilentlyContinue | out-null
	#echo "testing again connection"
	try{
		$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationLevel quiet
	} catch {
		# offline
	}
}
Write-Host -ForegroundColor Yellow "Make sure that now the system is online."


#update properties file.process_file
# skip script #5 which is user
$secPropsProcessFile=[int]$secPropsProcessFile
$secPropsProcessFile=++$secPropsProcessFile
updateScriptIndex($secPropsProcessFile)

$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
$users=$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
	$groups = $_.Groups() | Foreach-Object {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)}
	$_ | Select-Object @{n='UserName';e={$_.Name}},@{n='Groups';e={$groups -join ';'}}
}
foreach ($user in $users){
	if ($user.Groups -eq 'Users'){
		foreach ($user in $users){
			if ($user.Groups -eq 'Users'){
				createUserAutorunTask $user.username
			}
		}
	}
}

createAutorunTask
write-host -ForegroundColor yellow "Login to a Standard user account for Live Updating."
Write-Host
Read-Host "Press enter to exit..."
logoff
