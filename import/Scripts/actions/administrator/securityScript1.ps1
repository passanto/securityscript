#Requires -RunAsAdministrator


###################
# BEGIN
###################


<#
Set current network to public
#>
try {
	if (!$restartedPostPU){
		Write-Host
	    Write-Host -foregroundcolor red "Set current network to public? (Y/N):"
		$publicNetProfile = Read-Host -Prompt "Suggested: Yes"
	    if ($publicNetProfile -eq 'Y' -or $publicNetProfile -eq 'y'){
			$profile=Get-NetConnectionProfile
			if ($profile){
				if ($profile.NetworkCategory -ne "Public"){
					Set-NetConnectionProfile -Name $profile.Name -NetworkCategory Public
					Write-Host $profile.Name " network profile has been set to public" -ForegroundColor Yellow
				}else{
					Write-Host -ForegroundColor Yellow $profile.Name "network interface is already public"
				}
			} else {
				Write-Host -ForegroundColor Yellow "No network interface detected."
				read-host "Press ENTER to continue..."
			}
		}
	}
} catch {
	errorManagement $_ "Setting current network to public"
}


<#
disconnection if external system is available
#>
try{
	if (!$restartedPostPU){
		Write-Host
		if ($externalSystemAvailable -eq $false){
			Write-Host -ForegroundColor Yellow "Next step is to perform an offline update, if required."
			Write-Host
			Write-Host "If is it available an external system, this one will be disconnected from the network"
			Write-Host -ForegroundColor red "Is it available an external system connected to the network? (Y/N): "
			$extAvailable = Read-Host "Suggested Yes "
			Write-Host
			if ($extAvailable -eq 'Y' -or $extAvailable -eq 'y'){
				$externalSystemAvailable=$true
				updatePU "external_system_available" "true"
			} else {
				Write-Host -ForegroundColor Yellow "The system will be disconnected after the offline updated."
			}
		}
		if ($externalSystemAvailable -eq $true){
			$testNet = $false
			try{
				$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationAction SilentlyContinue -InformationLevel quiet
			} catch {
				<#Write-Host -ForegroundColor Yellow "The system seems not connected to internet."
				Write-Host -ForegroundColor Yellow "Have an external system connected to the network or try to connect this one."
				Read-Host 'Press Enter to continue'#>
			}
			while ($testNet -eq $true)
			{
				Write-Host -ForegroundColor Yellow "The system seems connected to internet"
				Write-Host -ForegroundColor Yellow "It is strongly reccomended to diconnect from the network"
				Write-Host -ForegroundColor Red "Automatically disconnect from the network? (Y/N)"
				$continueActive = Read-Host "Suggested Yes "
				if ($continueActive -ne 'Y' -or $continueActive -ne 'y'){
					Write-Host  "As you wish :)"
					break
				} else {
					Read-Host 'Press Enter to automatically disconnect all the network adapters'
					Get-NetAdapter | Disable-Netadapter -Confirm:$false -ErrorAction SilentlyContinue | out-null
				try{
					$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue  -InformationAction SilentlyContinue -InformationLevel quiet
				} catch {
					$testNet = $false
				}
					Write-Host -ForegroundColor Yellow "Make sure that now the system is offline."
					Write-Host
					#updatePU "system_disconnected" "true"
				}
			}
		}
	}
} catch {
	errorManagement $_ "disconnectIfConnected"
}


<#
Activate Windows License
#>
try{
	if (!$restartedPostPU){
		$skip=$false
		while  ( ($skip -eq $false) -and  ((Get-ActivationStatus).status -ne "Licensed") ){
			while ($key -eq $NULL) {
				Write-Host -ForegroundColor yellow "Enter the license key (with -dashes-):"
				$key=read-host
			}
			if (! [string]::IsNullOrEmpty($key) ){
				$service = get-wmiObject -query "select * from SoftwareLicensingService" -computername $env:computername
				try{
					$res=$service.InstallProductKey($key)
					$service.RefreshLicenseStatus()
				} catch {
					Write-Host -ForegroundColor red "Error while activating."
					$key = $NULL
				}
			} else {
				$res=read-host "Retry license activation?"
				if (! ($res -like 'Y') ){
					$skip=$true
				} else {
					$key = $NULL
				}
			}
		}
	}
} catch {
    errorManagement $_ "Activate Windows License"
		Write-Host  -ForegroundColor red "Consider activating Windows license now!"
}

<#
Recovery Drive
#>
try{
	if (!$restartedPostPU){
		Write-Host
		Write-Host -foregroundcolor yellow "If a Windows installer medium is available, this step can be skipped..."
	  Write-Host -foregroundcolor red "Create a Recovery Drive? (Y/N)"
	  $recoveryDriveCreation = Read-Host -Prompt "Yes, if no installer drive is available"
		if ($recoveryDriveCreation -like 'Y'){
			Write-Host -foregroundcolor yellow "Insert a USB drive of at least 8GB"
			Write-Host -foregroundcolor red "The USB drive will be the Emergency Recovery Drive."
			Write-Host -foregroundcolor red "The USB drive will be formatted!!!"
			Read-Host 'Press Enter to continue if the usb has been inserted...'
		  Write-Host -foregroundcolor yellow "The Windows native programm will now be executed in order to create the recovery drive"
		  Write-Host -foregroundcolor yellow "It is suggested to uncheck the 'backup' checkbox"
			Read-Host 'Press enter to start the Recovery Drive program...'
			start-process ${env:systemroot}\system32\RecoveryDrive.exe -nonewwindow -wait
			Read-Host 'Press Enter to continue if Recovery Drive has completed...'
			Write-Host -foregroundcolor red "It is suggested to use the drive for Recovery purposes only!"
			Write-Host -foregroundcolor yellow "The Recovery Drive can now be removed."
		}
	}
} catch {
    errorManagement $_ "Recovery Drive Creation"
}


<#
system image
#>
try{
	if (!$restartedPostPU){
			Write-Host
	    Write-Host -foregroundcolor red "Create a System Image? (Y/N):"
	    $systemImageCreation = Read-Host -Prompt "Suggested: Yes"
			if ($systemImageCreation -eq 'Y' -or $systemImageCreation -eq 'y'){
				Write-Host -foregroundcolor yellow "Will now be created a System Image."
				Write-Host -foregroundcolor yellow "Insert a 'big' USB drive"
				$systemDrive=${env:systemdrive}

				$choice=$false
				while ($choice -ne $true){
					$drives = GET-WMIOBJECT -class win32_logicaldisk
					foreach ($drive in $drives){
						if ($drive.DeviceID -ne $systemDrive){
							$driveId=$drive.DeviceID
							$driveIdChoosen=Read-Host -Prompt "Use the $driveId as Image Drive?"
							if ($driveIdChoosen -like 'Y'){
								$choice=$true
								break
							}
						}
					}
					$res=read-host "Retry choosing an external drive?"
					if (!($res -like 'Y')){
						$choice=$true
					} else {
						Write-Host -backgroundcolor red "Skipping System Image creation..."
					}
				}
				if ($driveIdChoosen -like 'Y'){
					Write-Host -foregroundcolor red "Press Enter to create the image drive on $driveId..."
					Read-Host "This will take some time..."
					wbAdmin start backup -backupTarget:$driveId -include:$systemDrive -allCritical #-quiet
					$SysImageFolder=(get-item -path "$driveId\WindowsImageBackup").fullname
					Rename-Item $SysImageFolder WindowsImageBackup_FreshInstall
					Write-Host -foregroundcolor red "Image drive created at: $driveId\WindowsImageBackup_FreshInstall."

					$vol = get-wmiobject -Class Win32_Volume | where{$_.DriveLetter -eq $driveId}
					$Eject =  New-Object -comObject Shell.Application
					$Eject.NameSpace(17).ParseName($vol.driveletter).InvokeVerb("Eject")
					Read-Host "Remove the USB drive and press Enter to continue"
				}
			} else {
				Write-Host -foregroundcolor red "No System Image will be created."
			}
	}
} catch {
	errorManagement $_ "System image creation"
}


<#
Enable and create restore point
#>
try {
	if (!$restartedPostPU){
		Write-Host
	  Write-Host -foregroundcolor red "Create a restore point? (Y/N):"
		$restorePointCreation = Read-Host -Prompt "Suggested: Yes"
	  if ($restorePointCreation -eq 'Y' -or $restorePointCreation -eq 'y'){
			$restorePointDescription="PreSecurityProcess"
			$driveLetter = ${env:SystemDrive}
			$drive=$driveLetter+"\"
			Enable-ComputerRestore -Drive $drive
		  Write-Host "A restore point for drive '$drive' named '$restorePointDescription' will be created"
		  Write-Host -NoNewline "Type a different name if required: "
		  $newRestorePointDescription = Read-Host
		  if ($newRestorePointDescription -ne ''){
		      $restorePointDescription=$newRestorePointDescription
		  }
		  $res=Checkpoint-Computer -Description $restorePointDescription | Out-Null
		  if ($res -eq $null){
		      Write-Host -ForegroundColor Yellow "Restore point created"
		  }
		}
	}
} catch {
	errorManagement $_ "Restore point creation"
}


<#
offline update
#>
try{
	if (!$restartedPostPU){
		if ($portableUpdateRequired -eq "true"){
			write-host -ForegroundColor Yellow "If this is a fresh installed system an offline update is suggested."
			$offileUpdate=Read-Host "Perform offline update? (Y/N)"
			Write-Host
			if ($offileUpdate -eq 'Y' -or $offileUpdate -eq 'y'){
					#initialize: copy to external
					write-host -ForegroundColor red "Portable Update will be used for update the system in offline mode."
					Write-Host
					initializePU
					Write-Host
					#scan and download: scan on external drive
					write-host -ForegroundColor red "Portable Update must have been initialized on an external system."
					write-host "Will now be executed on this system in order to scan it."
					Read-Host "Press Enter when Portable Update has been initialized and the drive inserted..."
					scanAndDownloadPU

					# complete: install the updates
					write-host -ForegroundColor red "Portable Update has downloaded required updates(!?!)."
					write-host "Will now be executed on this system in order to install the updates."
					write-host -fore red "Script will automatically reboot if required!!!"
					Read-Host "Press Enter to continue..."
					completePU

					# prompt for restart
					if (Test-PendingReboot){
							write-host -ForegroundColor red "System need to be restart due to updates."
							Read-Host "Press Enter to restart..."
							updateScriptProperties "restart_due_to_pu" "true"
							<#
								update restart_post_pu_done
							#>
							Restart-Computer -force
					}
			}
		}
	}
} catch {
    Write-Warning $_
    Write-Host -BackgroundColor Yellow -ForegroundColor Yellow "Something went wrong while performing Offline Update"
		Write-Host -ForegroundColor Red "It is strongly reccomended to restart the process and update!."
    Write-Host -BackgroundColor Red -ForegroundColor Yellow "Restart? (Y/N): "
    $restart = Read-Host
    if ($restart -ne 'Y' -or $restart -ne 'y'){
        return
    }
}


<#
disconnection if required after offline update
#>
try{
	$testNet = $false
	try{
		$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationLevel quiet
	} catch {
		Write-Host -ForegroundColor Yellow "The system should already be disconnected."
	}
	while ($testNet -eq $true)
	{
		Write-Host -ForegroundColor Yellow "The system seems connected to internet"
		Write-Host -ForegroundColor Yellow "It is strongly reccomended to diconnect from the network"
		Write-Host -ForegroundColor Red "Automatically disconnect from the network? (Y/N)"
		$continueActive = Read-Host "Suggested Yes "
		if ($continueActive -ne 'Y' -or $continueActive -ne 'y'){
			$continueActive = Read-Host "Automatically disconnect from the network? (Y/N)"
			if ($continueActive -ne 'Y' -or $continueActive -ne 'y'){
				break
			}
			} else {
			Read-Host 'Press Enter to automatically disconnect all the network adapters'
			#echo "disconnecting all the adaptes"
			Get-NetAdapter | Disable-Netadapter -Confirm:$false -ErrorAction SilentlyContinue | out-null
			#echo "testing again connection"
			try{
				$testNet = test-netconnection "www.google.com" -CommonTCPPort HTTP -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationLevel quiet
			} catch {
				# offline
			}
			Write-Host -ForegroundColor Yellow "Make sure that now the system is offline."
		}
	}
} catch {
    Write-Warning $_
	errorsLog $_
    Write-Host -BackgroundColor Yellow -ForegroundColor Yellow "Something went wrong while disconnecting"
	Write-Host -ForegroundColor Red "Make sure that now the system is offline."
    Write-Host -BackgroundColor Red -ForegroundColor Yellow "Continue? (Y/N): "
    $continue = Read-Host
    if ($continue -ne 'Y' -or $continue -ne 'y'){
        return
    }
}


<#
standard user creation
#>
try {
	$usersUserAlreadyPresent=$false
	$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
	$users=$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
	    $groups = $_.Groups() | Foreach-Object {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)}
	    $_ | Select-Object @{n='UserName';e={$_.Name}},@{n='Groups';e={$groups -join ';'}}
	}
	#check standard user already exists
	foreach ($user in $users){
		if ($user.Groups -eq 'Users'){
			write-host $user.username"is a stardard user"
			$res=read-host "Will be the regular user?"
			if ($res -like 'Y'){
				$usersUserAlreadyPresent=$true
				$userName=$user.username
			}
		}
	}
	if ($usersUserAlreadyPresent -eq $false){
		$confirmUserName=$false
		$usernameAlreadyExists=$false
		$usernameOk=$false
		while ($confirmUserName -eq $false){
			while ($usernameOk -eq $false){
				write-host -foregroundcolor yellow "No standard user found."
				write-host "Will be added now..."
				$userName=read-host "Enter the new user's name"
				if ( ($userName.length -lt 64) -AND ($userName -match '[A-Za-z0-9]') ) {
					#check username already exists
					foreach ($user in $users){
						if ($user.username -eq $userName){
							write-host "`'$userName`' user already exists"
							$usernameAlreadyExists=$true
							break
						}
					}
					if ($usernameAlreadyExists -eq $false){
						$res=read-host "Confirm $username as the daily regular user?"
						if (!($res -like 'N')){
							$confirmUserName=$true
							$usernameOk=$true
						}
						<#$userPassword=checkPassword
						& NET USER $userName $userPassword /add /y /expires:never#>
						$userPWDOk=$false
						while ($userPWDOk -eq $false){
							Write-Host -foregroundcolor red "WARING! THE PASSWORD WILL BE VISIBLE SINCE WILL BE CHANGED LATER ON"
							Write-Host -foregroundcolor yellow "Enter a temporary password for the standard user '$username'"
							$standardUserPassword=Read-Host
							$standardUserPasswordConfirm=Read-Host "Confirm password for the standard user"
							if ($standardUserPasswordConfirm -eq $standardUserPassword){
								& NET USER $userName $standardUserPassword /add /y /expires:never
								$userPWDOk = $true
							} else {
								Write-Host -foregroundcolor red "PASSWORD AND CONFIRM DONT MATCH"
							}
						}
					#} else {
						#write-host -foregroundcolor red "$username is not valid"
					}
				}
			}
		}
	}
	if ([System.IntPtr]::Size -eq 4) {
		$psexec="$UtilsFolderRelPath\PsExec.exe"
	}else{
		$psexec="$UtilsFolderRelPath\PsExec64.exe"
	}
	$command="$psexec -accepteula -d -u `"$userName`" -p `"$standardUserPassword`" `"cmd.exe`" | out-null"
	Invoke-Expression -Command:$command 2>&1
	$p=Get-Process "cmd" -IncludeUserName
	if ($p -ne $null){
		if ($p.UserName.EndsWith($userName)){
			$p.Kill()
		}
	}

	write-host -foregroundcolor yellow "`'$userName`' has to be the daily user!!!"
} catch {
	errorManagement $_ "standard user creation"
}


<#
Set control panel to always show large icons
#>
try {
	Write-Host
    Write-Host -foregroundcolor red "Set control panel to always show large icons? (Y/N):"
	$controlPanelLargeIcons = Read-Host -Prompt "Suggested: Yes"
    if ($controlPanelLargeIcons -eq 'Y' -or $controlPanelLargeIcons -eq 'y'){
		$controlPanelPath="HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer"
		$uacKey="ForceClassicControlPanel"
		$uacKeyValue=1
		Set-ItemProperty -Path $controlPanelPath -Name $uacKey -Value $uacKeyValue -ErrorAction Continue
		Write-Host -ForegroundColor Yellow "Control panel will show large icons"
	}
} catch {
	errorManagement $_ "Control Panel to always show large icons"
}


<#
Set UAC to max:
#>
try {
	Write-Host
    Write-Host -foregroundcolor red "Set User Account Control to maximum? (Y/N):"
	$acuMax = Read-Host -Prompt "Suggested: Yes"
    if ($acuMax -eq 'Y' -or $acuMax -eq 'y'){
		$uacPath="HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
		$uacKey="ConsentPromptBehaviorAdmin"
		$uacKeyValue=2
		Set-ItemProperty -Path $uacPath -Name $uacKey -Value $uacKeyValue -ErrorAction Continue
		Write-Host -ForegroundColor Yellow "UAC has been set to max"
	}
} catch {
	errorManagement $_ "UAC maximize"
}



<#
Disabling Network protocols
#>
try {
	Write-Host
	Write-Host "Choose the network protocols to leave enabled? (Y/N)"
	$choose = Read-Host -Prompt "If not, all not necessary protocols will be disabled"
	$components=('ms_msclient','ms_server', 'ms_pacer', 'ms_implat', 'ms_lldp', 'ms_lltdio', 'ms_rspndr', 'ms_tcpip6')
	$adapters=(Get-NetAdapter | Select-Object Name)
	foreach ($adapter in $adapters.name) {
		foreach($component in $components){
			if ($choose -like 'Y'){
				Write-Host -foregroundcolor red "Disable '$component' for '$adapter'?"
				$disable = Read-Host -Prompt "Suggested: Yes"
			}
			if ($disable -or !($choose -like 'Y') ){
				Disable-NetAdapterBinding -Name $adapter -ComponentID $component
				Write-Host -ForegroundColor Yellow "Disabled $component network protocol for the $adapter adapter"
			}
		}
	}
} catch {
    errorManagement $_ "Disabling Network protocols"
}


<#
Disabling DNS registration
#>
try {
	Write-Host
	Write-Host -foregroundcolor red "Disable DNS cache? (Y/N):"
	$dns = Read-Host -Prompt "Suggested: Yes"
    if ($dns -eq 'Y' -or $dns -eq 'y'){
		$enabledAdapters=Get-WmiObject -Class Win32_NetworkAdapter -Filter "NetEnabled=True"
		foreach($enabledAdapter in $enabledAdapters){
			$enabledAdapterConf=$enabledAdapter.GetRelated('Win32_NetworkAdapterConfiguration')
			$enabledAdapterConf.SetDynamicDNSRegistration($false,$false)  | Out-Null
			Write-Host -ForegroundColor Yellow  "Disabled DNS registration for" $enabledAdapter.Name
		}
	}
} catch {
    errorManagement $_ "Disabling DNS registration"
}


<#
Disabling Net BIOS
#>
try {
	Write-Host
	Write-Host -foregroundcolor red "Disable NetBIOS? (Y/N):"
	$netBios = Read-Host -Prompt "Suggested: Yes"
    if ($netBios -eq 'Y' -or $netBios -eq 'y'){
		$adapters=(gwmi win32_networkadapterconfiguration)
		Foreach ($adapter in $adapters){
			$adapter.settcpipnetbios(2) | Out-Null
			Write-Host -ForegroundColor Yellow  "Disabled NetBIOS for" $adapter.description
		}
	}
} catch {
	errorManagement $_ "Disabling Net BIOS"
}


<#
Disabling IPv6 on registry
#>
try {
	Write-Host
	Write-Host -foregroundcolor red "Disable IPv6? (Y/N):"
	$ipv6 = Read-Host -Prompt "Suggested: Yes"
	if ($ipv6 -eq 'Y' -or $ipv6 -eq 'y'){
		Write-Host 'Is your router IPv6 compliant? (Y/N):'
		$ipv6Router = Read-Host -Prompt '(most routers are IPv4 - suggested: No)'
		if ($ipv6Router -eq 'Y' -or $ipv6Router -eq 'y'){
			$ipv6KeyValue=1
		} else {
			$ipv6KeyValue=255
		}
		$ipv6KeyPath="HKLM:\SYSTEM\CurrentControlSet\Services\tcpip6\Parameters"
		$ipv6Key="DisabledComponents"
		Set-ItemProperty -Path $ipv6KeyPath -Name $ipv6Key -Value $ipv6KeyValue -ErrorAction Continue | Out-Null
		New-ItemProperty -Path $ipv6KeyPath -Name $ipv6Key -Value $ipv6KeyValue -PropertyType DWORD -ErrorAction SilentlyContinue | Out-Null
		Write-Host -ForegroundColor Yellow  "Disabled IPv6 on registry"
	}
} catch {
	errorManagement $_ "Disabling Net IPv6"
}


<#
Disable unused tcpipv6 devices
#>
try{
	Write-Host
	Write-Host -foregroundcolor red "Disable unused TCPIPv6 devices? (Y/N):"
	$tcpipv6 = Read-Host -Prompt "Suggested: Yes"
	if ($tcpipv6 -eq 'Y' -or $tcpipv6 -eq 'y'){
		$mkdna=Get-PnpDevice -FriendlyName 'Microsoft Kernel Debug Network Adapter'
		#$rddrb=Get-PnpDevice -FriendlyName 'Remote Desktop Device Redirector Bus'
		Disable-PnpDevice -Confirm:$false -InstanceId $mkdna.InstanceId
		Write-Host -ForegroundColor Yellow  "Microsoft Kernel Debug Network Adapter"
		$rddrbKeyPath="HKLM:\SYSTEM\CurrentControlSet\Enum\ROOT\RDPBUS\0000"
		$rddrbKey="ConfigFlags"
		$rddrbKeyValue=1
		Set-ItemProperty -Path $rddrbKeyPath -Name $rddrbKey -Value $rddrbKeyValue -ErrorAction Continue | Out-Null
		#Disable-PnpDevice -Confirm:$false -InstanceId $rddrb.InstanceId -ErrorAction Continue
		Write-Host -ForegroundColor Yellow  "Remote Desktop Device Redirector Bus"
	}
} catch {
	errorManagement $_ "Disabling unused TCPIPv6 devices"
}


Write-Host
#update properties file.process_file
updateScriptIndex($secPropsProcessFile)
createAutorunTask
write-host -ForegroundColor red "System needs to be restarted"
write-host -ForegroundColor yellow "The process will start automatically"
Write-Host
Read-Host "Press enter to reboot..."
Restart-Computer -force
