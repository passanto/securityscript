#Requires -RunAsAdministrator

###################
# BEGIN
###################


<#
Disable IGMP
#>
try{
	Write-Host
	Write-Host -foregroundcolor red "Disable IGMP? (Y/N):"
	$IGMP = Read-Host -Prompt "Suggested: Yes"
    if ($IGMP -eq 'Y' -or $IGMP -eq 'y'){
		if ( (Get-NetIPv4Protocol).IGMPlevel -eq 'None' ) {
			write-host -ForegroundColor Cyan "IGMPlevel is already disabled"
		} else {
			Set-NetIPv4Protocol -IGMPLevel none -passthru -OutVariable result | Out-Null
			if ($result.IGMPLevel -eq 'None') {
				write-host -ForegroundColor Yellow "IGMPLevel disabled"
			} else {
				write-warning "Failed to change IGMPLevel"
			}
		}
	}
} catch {
	errorManagement $_ "Disabling IGMP"
}


<#
Disable port 1900 UPnP
#>
try{
	Write-Host
	Write-Host -foregroundcolor red "Disable UPnP? (Y/N):"
	$UPnP = Read-Host -Prompt "Suggested: Yes"
    if ($UPnP -eq 'Y' -or $UPnP -eq 'y'){
		$UPPath = "HKLM:Software\Microsoft\DirectplayNATHelp\DPNHUPnP"
		$uppName = "UPnPMode"
		$uppValue = 2
		Set-ItemProperty -Path $UPPath -Name $uppName -Value $uppValue -ErrorAction Continue
		New-ItemProperty -Path $UPPath -Name $uppName -Value $uppValue -PropertyType DWORD -ErrorAction SilentlyContinue
		write-host -ForegroundColor Yellow "UPnP disabled"
	}
} catch {
	errorManagement $_ "Disabling UPnP"
}


<#
Disable SMB v1 protocol
#>
try{
	Write-Host
	Write-Host -foregroundcolor red "Disable SMB v1 ? (Y/N):"
	$SMBv1 = Read-Host -Prompt "Suggested: Yes"
    if ($SMBv1 -like 'Y'){
		$smb1=Get-WindowsOptionalFeature -online -FeatureName SMB1Protocol
		if ($smb1.State -eq "Disabled"){
			write-host -ForegroundColor Yellow "SMB v1 is already disabled"
		} else {
			Disable-WindowsOptionalFeature -Online -FeatureName SMB1Protocol -NoRestart -WarningAction SilentlyContinue | out-null
			write-host -ForegroundColor Yellow "SMB v1 disabled"
			write-host -ForegroundColor red "Reboot the system and continue..."
		}
	}
} catch {
	errorManagement $_ "Disabling SMB v1"
}

Write-Host
#update properties file.process_file
updateScriptIndex($secPropsProcessFile)
createAutorunTask
write-host -ForegroundColor red "System needs to be restarted"
write-host -ForegroundColor yellow "The process will start automatically"
Write-Host
Read-Host "Press enter to reboot..."
Restart-Computer -force
