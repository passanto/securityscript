#Requires -RunAsAdministrator

###################
# BEGIN
###################


<#
baseline
#>
try{
	write-Host
	write-Host -foregroundColor green "Baseline generation..."
	$res=read-host "Generate baselines?"
	if ($res -like 'Y'){
		$date = Get-Date -Format "yyyy-M-d_H.m"
		#netstat
		netstat.exe -abn >  "$OutputsFolder\baselines\netstat\$date.txt"
		#processes list
		Get-Process | Sort Name | select Name, PriorityClass, FileVersion, Path, Company, Description, Product  | format-table -AutoSize > "$OutputsFolder\baselines\processes\$date.txt"
		#autoruns
		if ([System.IntPtr]::Size -eq 4) {
			$autoruns = $UtilsFolder+"\autoruns.exe"
		} else {
			$autoruns = $UtilsFolder+"\autoruns64.exe"
		}
		write-Host 'Autoruns will automatically create a baseline.'
		write-Host 'Please wait for its completion'
		Read-Host -Prompt 'Press Enter to continue'
		$autorunsBaseline = "-accepteula -a `"$OutputsFolder\baselines\autoruns\$date.arn`""
		start-process $autoruns -ArgumentList $autorunsBaseline -nonewwindow -wait
		#driverquery
		driverquery.exe > $OutputsFolder\baselines\driverquery\$date.txt
		write-host -foregroundcolor green "Baselines created at $OutputsFolder\baselines"
	}
} catch {
	errorManagement $_ "Baselines"
}


<#
sfc
#>
try{
	write-host
	$deleteFwRule = Read-Host -Prompt 'Run System File Checker?'
	if ($deleteFwRule -eq 'Y' -or $deleteFwRule -eq 'Y'){
		sfc.exe /scannow
	}
} catch {
	errorManagement $_ "Baselines"
}


<#
acls and security
#>
try {
	write-host
	Write-Host "Apply systemroot ACLs? "
	$pwdPolicies = Read-Host
	if ($pwdPolicies -like 'Y'){
		$infFile = "$ConfFolder\acls.inf"
		$sdbFile="$OutputsFolder\acls.sdb"
	  secedit /configure /db $sdbFile /cfg $infFile | Out-Null
		Write-Host  -ForegroundColor Yellow "Systemroot ACLs applied."
		Write-Host  -ForegroundColor Yellow "Sensitive programs have been granted to Administrators users only."
	}

	write-host
	Write-Host "Apply security policies? "
	$pwdPolicies = Read-Host
	if ($pwdPolicies -like 'Y'){
		$infFile = "$ConfFolder\security_policies.inf"
		$sdbFile="$OutputsFolder\security_policies.sdb"
	  secedit /configure /db $sdbFile /cfg $infFile | Out-Null
	  Write-Host  -ForegroundColor Yellow "Security policies applied."
	}
	Write-Host
	$res=read-host "Do you want to hide last logged in user on logon screen?"
	$logonUserPath="HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
	$logonUserKey="dontdisplaylastusername"
	if ($res -like 'Y'){
		Set-ItemProperty -Path $logonUserPath -Name $logonUserKey -Type DWord -Value 1
	} else {
		Set-ItemProperty -Path $logonUserPath -Name $logonUserKey -Type DWord -Value 0
	}
	Write-Host
	$res=read-host "Do you want to restrict standard user from request for administrators privilege?"
	$logonUserKey="ConsentPromptBehaviorUser"
	if ($res -like 'Y'){
		Set-ItemProperty -Path $logonUserPath -Name $logonUserKey -Type DWord -Value 0
	} else {
		Set-ItemProperty -Path $logonUserPath -Name $logonUserKey -Type DWord -Value 1
	}
} catch {
	errorManagement $_ "ACLs"
}


#update properties file.process_file
$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
$users=$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
	$groups = $_.Groups() | Foreach-Object {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)}
	$_ | Select-Object @{n='UserName';e={$_.Name}},@{n='Groups';e={$groups -join ';'}}
}

<#foreach ($user in $users){
	if ($user.Groups -eq 'Users'){
		foreach ($user in $users){
			if ($user.Groups -eq 'Users'){
				createUserAutorunTask $user.username
			}
		}
	}
}#>

# createAutorunTask
# . $ScriptsFolder\actions\user\securityScript99.ps1



# aux functions
function createUpdateKeyValue($path, $key, $value){
	New-ItemProperty -Path $path -Name $key -Value $value -PropertyType DWORD -Force | Out-Null
	Set-ItemProperty -Path $path -Name $key -Value $value -Force | Out-Null
}

function createUpdateKeyValueSZ($path, $key, $value){
	New-ItemProperty -Path $path -Name $key -Value $value -PropertyType String -Force | Out-Null
	Set-ItemProperty -Path $path -Name $key -Value $value -Force | Out-Null
}



<#
CHANGE pwd
#>
try {
	write-host
	write-host "It's time to set a secure password."
	write-host "Consider using a password manager..."
	$resPwds=read-host "Leave passwords unsecured?"
	if (!($resPwds -like 'Y')){
		$userslist  = get-wmiobject win32_useraccount
		$users=Get-LocalUser
		$localUsers=@()
		foreach($user in $users){
			if ($user.Enabled -eq $true){
				$localUsers+=$user.name
			}
		}
		foreach($userIndex in $userslist){
			if ($localUsers -contains $userIndex.name){
				changeUserPassword $userIndex
			}
		}
	}
} catch {
	errorManagement $_ "CHANGE pwd"
}


# registry users key array
try {
	$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
	$users=$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
		$groups = $_.Groups() | Foreach-Object {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)}
		$_ | Select-Object @{n='UserName';e={$_.Name}},@{n='Groups';e={$groups -join ';'}}
	}
	$regKeys=@("HKCU:")
	foreach ($user in $users){
		if ($user.Groups -eq 'Users'){
			$unameRegKey=$user.UserName
			New-PSDrive -PSProvider Registry -Name $unameRegKey -Root HKEY_USERS | out-null

			reg load "HKU\$unameRegKey"  "$env:systemdrive\Users\$unameRegKey\NTUSER.DAT" | out-null
			$regKeys+="Microsoft.PowerShell.Core\Registry::HKEY_USERS\$unameRegKey"
		}
	}
} catch {
	errorManagement $_ "registry users key array"
}


# users profile array
try {
	$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
	$users=$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
		$groups = $_.Groups() | Foreach-Object {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)}
		$_ | Select-Object @{n='UserName';e={$_.Name}},@{n='Groups';e={$groups -join ';'}}
	}
	$usersProfiles=@($env:userprofile)
	foreach ($user in $users){
		if ($user.Groups -eq 'Users'){
			$uname=$user.UserName
			$usersProfiles+="$env:systemdrive\Users\$uname"
		}
	}
} catch {
	errorManagement $_ "users profile array"
}


<#
onedrive removal
#>
try {
	Write-Host
	Write-Host -ForegroundColor Yellow 'Remove OneDrive? (Y/N):'
	$removeOneDrive = Read-Host -Prompt 'Strongly suggested yes'
	if ($removeOneDrive -like 'y'){
		# "Removing Administrators scheduled task"
		Get-ScheduledTask -TaskPath '\' -TaskName 'OneDrive*' -ea SilentlyContinue | Unregister-ScheduledTask -Confirm:$false

		Foreach ($user in $usersProfiles)
		{
			## "Removing startmenu entry"
			rm -Force -ErrorAction SilentlyContinue "$user\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\OneDrive.lnk"
			if (test-path "$user\OneDrive\"){

				$acl = Get-Acl "$user\OneDrive\"
				$permission  = "BUILTIN\Administrators","FullControl", "ContainerInherit,ObjectInherit","None","Allow"
				$rule = New-Object System.Security.AccessControl.FileSystemAccessRule $permission
				$acl.SetAccessRule($rule)
				$acl.SetAccessRuleProtection($True, $false)
				#Set-Acl $alterFolder $acl
				(Get-Item "$user\OneDrive\").SetAccessControl($acl)


				# user's one drive folder
				if ((gci $user\OneDrive\).length -gt 0){
					write-host "$user\OneDrive\ is not empy"
					write-host -BackgroundColor red "Save the files if required and then continue"
					read-host "Press enter to continue..."
				}
				Takeown-Folder $user\OneDrive\
				remove-Item $user\OneDrive\ -Force -Recurse
			}
		}
	}
} catch {
	errorManagement $_ "onedrive removal user"
}


<#
enable Explorer file visibility
#>
try {
	Write-Host
	Foreach ($regKey in $regKeys)
	{
		$keyAdvanced = "$regKey\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
		$keyCabinetState = "$regKey\Software\Microsoft\Windows\CurrentVersion\Explorer\CabinetState"

		if (! (test-path $keyAdvanced)){
			new-item $keyAdvanced -force | Out-Null
		}
		if (! (test-path $keyCabinetState)){
			new-item $keyCabinetState -force  | Out-Null
		}

		# 1. Always show menu $keyAdvanced
		$name = "AlwaysShowMenus"
		$value = "1"
		createUpdateKeyValue $keyAdvanced $name $value

		#  2. Full path $keyCabinetState
		$name = "FullPath"
		$value = "1"
		createUpdateKeyValue $keyCabinetState $name $value

		# keyCabinetState\FullPathAddress 1
		$name = "FullPathAddress"
		$value = "1"
		createUpdateKeyValue $keyCabinetState $name $value

		# 3. Enabling showing hidden files keyAdvanced\Hidden 1
		$name = "Hidden"
		$value = "1"
		createUpdateKeyValue $keyAdvanced $name $value

		# 4. show empty drive
		### keyAdvanced\HideDrivesWithNoMedia 0
		$name = "HideDrivesWithNoMedia"
		$value = "0"
		createUpdateKeyValue $keyAdvanced $name $value

		# 5. show folder merge conflicts
		# $ keyAdvanced\HideMergeConflicts 0
		$name = "HideMergeConflicts"
		$value = "0"
		createUpdateKeyValue $keyAdvanced $name $value

		# 6. extensions for known files keyAdvanced\HideFileExt 0
		$name = "HideFileExt"
		$value = "0"
		createUpdateKeyValue $keyAdvanced $name $value

		# 7 showing hidden operation system files keyAdvanced\ShowSuperHidden 1
		$name = "ShowSuperHidden"
		$value = "1"
		createUpdateKeyValue $keyAdvanced $name $value

		# 8 never combine taskbat buttons keyAdvanced\TaskbarGlomLevel 2
		$name = "TaskbarGlomLevel"
		$value = "2"
		createUpdateKeyValue $keyAdvanced $name $value


		<# drop down menu...
		# 8 showing files name exensions
		# $keyAdvanced\HideFileExt 0
		$name = "HideFileExt"
		$value = "0"
		createUpdateKeyValue $keyAdvanced $name $value

		# 9 hidden items
		# $keyAdvanced\Hidden 1
		$name = "Hidden"
		$value = "1"
		#>
	}
	# "Restarting explorer shell to apply registry changes"
	Stop-Process -processname explorer

	write-Host -foregroundcolor red "Windows Explorer set to show useful information"
} catch {
	errorManagement $_ "Windows Explorer"
}


<#
Disabling autorun
#>
try {
	Write-Host
	Foreach ($regKey in $regKeys)
	{
		#autoplay
		Set-ItemProperty -Path "$regKey\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers" -Name "DisableAutoplay" -Type DWord -Value 1
		#autorun
		If (!(Test-Path "$regKey\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer")) {
			New-Item -Path "$regKey\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" | Out-Null
		}
		Set-ItemProperty -Path "$regKey\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -Name "NoDriveTypeAutoRun" -Type DWord -Value 255
	}
 	Write-Host -ForegroundColor Yellow "Autoplay has been deactivated"
	Write-Host -ForegroundColor Yellow "Consider check and disable it also manually"
} catch {
	errorManagement $_ "Disabling autorun"
}


<#
screen saver
#>
try {
	Write-Host
	Foreach ($regKey in $regKeys)
	{
		# enable
		createUpdateKeyValueSZ "$regKey\Control Panel\Desktop" "ScreenSaveActive" 1
		#Reg add "$regKey\Control Panel\Desktop" /v ScreenSaveActive /t REG_SZ /d 1 /f

		# blank
		createUpdateKeyValueSZ "$regKey\Control Panel\Desktop" "SCRNSAVE.EXE" "C:\Windows\system32\scrnsave.scr"
		#reg add "$regKey\Control Panel\Desktop" /v SCRNSAVE.EXE /t REG_SZ /d C:\Windows\system32\scrnsave.scr /f

		# timeout
		createUpdateKeyValueSZ "$regKey\Control Panel\Desktop" "ScreenSaveTimeOut" 600
		#Reg add "$regKey\Control Panel\Desktop" /v ScreenSaveTimeOut /t REG_SZ /d 600 /f

		# pwd
		createUpdateKeyValueSZ "$regKey\Control Panel\Desktop" "ScreenSaverIsSecure" 1
		#Reg add "$regKey\Control Panel\Desktop" /v ScreenSaverIsSecure /t REG_SZ /d 1 /f
	}
	write-Host -foregroundcolor red "Screen saver has been set as:"
	write-Host -foregroundcolor yellow "-enabled"
	write-Host -foregroundcolor yellow "-10 minutes timeout"
	write-Host -foregroundcolor yellow "-back to login"
} catch {
	errorManagement $_ "screen saver"
}


<#
Disable live tiles ????????????
#>
try {
	Write-Host
	Foreach ($regKey in $regKeys)
	{
		If (!(Test-Path "$regKey\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications"))
		{
			New-Item -Path "$regKey\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" | Out-Null
		}
		Set-ItemProperty -Path "$regKey\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" -Name "NoTileApplicationNotification" -Type DWord -Value 1
	}
	Write-Host -ForegroundColor Yellow "Live Tiles disabled"
} catch {
	errorManagement $_ "Disable live tiles"
}


<#
chml user's folder
#>
try{
	Write-Host
	Foreach ($user in $usersProfiles)
	{
		$username=$user.split("\")[2]
		Write-Host -ForegroundColor Yellow "Integrity $username's folder tunin:"
		$userFolders=Get-ChildItem $user | ?{ $_.PSIsContainer }
		write-Host "Set integrity protection for all $username's folders and sub-items?"
		$res = read-Host "Download folder will be set to low"
		if ($res -like 'Y'){
			$alterFolders = $userFolders.fullname
		} else {
			$alterFolders = New-Object System.Collections.ArrayList
			foreach ($userFolder in $userFolders){
				if($userFolder.name -notlike  "Downloads") {
					$userFolder=$userFolder.fullname
					$res = read-Host "Set integrity protection for $userFolder and sub-items? "
					if ($res -like 'Y'){
						$alterFolders.add($userFolder)
					}
				} else {
					$downloadFolder = $userFolder
				}
			}
		}

		$chml="$UtilsFolderRelPath\chml.exe "
		$chmlParams = " -i:m -nr -nw -nx"
		$alterFolders | ForEach-Object {
			takeown-folder $_

			#echo "Processing $_"
			$command="$chml `"$_`" $chmlParams"
			invoke-expression -Command:$command -ErrorAction Continue | out-null
			$items=(Get-ChildItem -recurse -LiteralPath $_ -ErrorAction SilentlyContinue)
			$items.fullname | ForEach-Object {
				if ($_ -ne $null){
					#echo "Processing $_"
					$command="$chml `"$_`" $chmlParams"
					invoke-expression -Command:$command -ErrorAction Continue | out-null
				}
			}
		}
		$chmlParams = " -i:l"
		#echo "Processing $downloadFolder"
		invoke-expression $chml"`"$downloadFolder"`"$chmlParams -ErrorAction Continue | out-null
		write-host ""
	}
} catch {
	errorManagement $_ "User CHML"
}


<#
user's ACL setting (user's-only access)
#>
try{
	Write-Host
	Foreach ($user in $usersProfiles)
	{
	$username=$user.split("\")[2]
	Write-Host -ForegroundColor Yellow "ACL $username's folder tunin:"
	$userFolders=Get-ChildItem $user | ?{ $_.PSIsContainer }

		$res = read-Host "Set User-Only ACL for all $username's folders and sub-items? "
		if ($res -like 'Y'){
			$alterFolders = $userFolders.fullname
		} else {
			$alterFolders = New-Object System.Collections.ArrayList
			foreach ($userFolder in $userFolders){
				$userFolder=$userFolder.fullname
				$res = read-Host "Set User-Only ACL for $userFolder and sub-items? "
				if ($res -like 'Y'){
					$alterFolders.add($userFolder)
				}
			}
		}
		<# process ACLs#>
		foreach ($alterFolder in $alterFolders){

			$acl = New-Object System.Security.AccessControl.DirectorySecurity
			$Group = New-Object System.Security.Principal.NTAccount($env:USERDOMAIN, $username)
			$permission  = "$env:USERDOMAIN\$username","FullControl", "ContainerInherit,ObjectInherit","None","Allow"
			$rule = New-Object System.Security.AccessControl.FileSystemAccessRule $permission

			$ACL.SetOwner($Group)
			$ACL.SetGroup($Group)
			$acl.SetAccessRule($rule)
			$acl.SetAccessRuleProtection($True, $false)

			set-acl $alterFolder $acl
			write-host "ACL set for '$alterFolder' "
		}
		write-host ""
	}
} catch {
	errorManagement $_ "User Acl"
}


<#
ie settings
#>
try{
	Foreach ($regKey in $regKeys)
	{
		#ActiveXFiltering
		$ieKeyGranParentPath = "HKCU:\Software\Microsoft\Internet Explorer"
		$ieKeyParentPath = "Safety"
		$ieKey="ActiveXFiltering"
		if ( (test-path $ieKeyGranParentPath\$ieKeyParentPath) -eq $false){
			New-Item -Path $ieKeyGranParentPath -Name $ieKeyParentPath | out-null
			New-Item -Path $ieKeyGranParentPath\$ieKeyParentPath -Name $ieKey | out-null
		}
		$name = "IsEnabled"
		$value = "1"
		createUpdateKeyValue $ieKeyGranParentPath\$ieKeyParentPath\$ieKey $name $value

		# disabe first run customizaion
		$ieKey = "HKCU:\Software\Microsoft\Internet Explorer\Main"
		$name = "DisableFirstRunCustomize"
		$value = "1"
		createUpdateKeyValue $ieKey $name $value

		#ie first run simulation
		Start-Process iexplore.exe
		$ieStop = $false
		while ($ieStop -eq $false){
			$ieProc = Get-Process iexplore -ErrorAction SilentlyContinue
			if ($ieProc -ne $NULL){
				$ieProc.CloseMainWindow() | out-null
			}
			if (!$ieProc.HasExited) {
				$ieProc | Stop-Process -Force | out-null
				$ieStop=$true
			}
		}

		# enhanced protected mode
		$name = "Isolation"
		$value = "PMEM"
		createUpdateKeyValueSZ $ieKey $name $value

		# enhanced protected mode 64it
		$name = "Isolation64Bit"
		$value = "1"
		createUpdateKeyValue $ieKey $name $value

		# ZONES
		$ieKey = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\1"
		$name = "2500"
		$value = "0"
		createUpdateKeyValue $ieKey $name $value

		$ieKey = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\2"
		createUpdateKeyValue $ieKey $name $value

		$ieKey = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3"
		createUpdateKeyValue $ieKey $name $value

		$ieKey = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\4"
		createUpdateKeyValue $ieKey $name $value



		$ieKey = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\1"
		createUpdateKeyValue $ieKey $name $value

		$ieKey = "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\2"
		createUpdateKeyValue $ieKey $name $value

		$ieKey = "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3"
		createUpdateKeyValue $ieKey $name $value

		$ieKey = "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\4"
		createUpdateKeyValue $ieKey $name $value
	}
} catch {
	errorManagement $_ "IE settings"
}


#############################################
#############################################
#############################################

foreach ($user in $users){
	if ($user.Groups -eq 'Users'){
		$unameRegKey=$user.UserName
		reg unload "HKU\$unameRegKey"  | out-null
		Remove-PSDrive $unameRegKey
	}
}



<#
Enable and create restore point
#>
try {
	Write-Host
  Write-Host -foregroundcolor red "Create a new restore point? (Y/N):"
	$restorePointCreation = Read-Host -Prompt "Suggested: Yes"
  if ($restorePointCreation -like 'Y'){
		$restorePointDescription="PostSecurityProcess"
		$driveLetter = ${env:SystemDrive}
		$drive=$driveLetter+"\"
		Enable-ComputerRestore -Drive $drive
	  Write-Host "A restore point for drive '$drive' named '$restorePointDescription' will be created"
	  Write-Host -NoNewline "Type a different name if required: "
	  $newRestorePointDescription = Read-Host
	  if ($newRestorePointDescription -ne ''){
	      $restorePointDescription=$newRestorePointDescription
	  }
	  $res=Checkpoint-Computer -Description $restorePointDescription | Out-Null
	  if ($res -eq $null){
	      Write-Host -ForegroundColor Yellow "Restore point created"
	  }
	}
} catch {
	errorManagement $_ "Enable and create restore point"
}


<#
post system image
#>
try{
	Write-Host
  Write-Host -foregroundcolor red "Create a Post Process System Image? (Y/N):"
  $systemImageCreation = Read-Host -Prompt "Suggested: Yes"
	if ($systemImageCreation -eq 'Y' -or $systemImageCreation -eq 'y'){
		Write-Host -foregroundcolor yellow "Will now be created a System Image."
		Write-Host -foregroundcolor yellow "Insert a 'big' USB drive"
		$systemDrive=${env:systemdrive}
		$drives = GET-WMIOBJECT -class win32_logicaldisk
		foreach ($drive in $drives){
			if ($drive.DeviceID -ne $systemDrive){
				$driveId=$drive.DeviceID
				$driveIdChoosen=Read-Host -Prompt "Use the $driveId as Image Drive?"
				if ($driveIdChoosen -like 'Y'){
					break
				}
			}
		}

		Write-Host -foregroundcolor red "Press Enter to create the image drive on driveId..."
		Read-Host "This will take some time..."
		wbAdmin start backup -backupTarget:$driveId -include:$systemDrive -allCritical #-quiet
		$SysImageFolder=(get-item -path "$driveId\WindowsImageBackup").fullname
		Rename-Item $SysImageFolder WindowsImageBackup_PostProcess
		Write-Host -foregroundcolor red "Image drive created at: $driveId\WindowsImageBackup_PostProcess."

		$vol = get-wmiobject -Class Win32_Volume | where{$_.DriveLetter -eq $driveId}
		$Eject =  New-Object -comObject Shell.Application
		$Eject.NameSpace(17).ParseName($vol.driveletter).InvokeVerb("Eject")
		Read-Host "Remove the USB drive and press Enter to continue"
	} else {
		Write-Host -foregroundcolor red "No System Image will be created."
	}
} catch {
	errorManagement $_ "post system image"
}



## process completed
Write-Host
Write-Host -ForegroundColor yellow "Consider checking Firewall rules!"
$open=Read-Host 'Open Windows Advanced Firewall?'
	if ($open -like 'Y'){
		& wf.msc
	}
read-host "Press enter to continue..."
checkFirewallRules
importMinimalAndBrowsersRules
importOptionalsRules

Write-Host
$bl=read-Host "Search paths to blacklist?"
if ($bl -like 'Y'){
	Write-Host "This can take a while..."
	findPathToDeny
}
Write-Host "Activating whitelisting lockdown"
lockSRP
checkSrpKeys

$netAds=Get-NetAdapter
foreach ($netAd in $netAds) {
	$name=$netAd.name
	$res = read-host "Enable '$name' network adapter?"
	if ($res -like 'Y'){
		Enable-Netadapter $name -Confirm:$false -ErrorAction SilentlyContinue | out-null
	}
}

# reset the script
$DefaultConfFolder=(get-item -path "import\Conf\default").fullname
copy-item -path "$DefaultConfFolder\securityScript.properties" -destination $ConfFolder

write-Host
write-Host -NoNewline 'Launch Autoruns for selecing startup applications? '
$autoruns=Read-Host
if ($autoruns -like 'Y'){
	if ([System.IntPtr]::Size -eq 4) {
		$autoruns = $UtilsFolder+"\autoruns.exe"
	} else {
		$autoruns = $UtilsFolder+"\autoruns64.exe"
	}
	start-process $autoruns -nonewwindow -wait
}


Write-Host
Write-Host -ForegroundColor red "Process completed"
Write-Host -ForegroundColor green "Enjoy :)"
read-host "Press enter to exit"
exit
