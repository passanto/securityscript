function importMinimalAndBrowsersRules(){
	$firewallScriptsFolder="$ScriptsFolder\firewall"
	$firewallScripts = get-childitem $firewallScriptsFolder -Filter *.ps1
	foreach ($firewallScript in $firewallScripts){
		. $firewallScript.FullName
	}
} #importMinimalAndBrowsersRules

function importOptionalsRules(){
	$additionalfirewallScriptsFolder="$ScriptsFolder\firewall\optionals"
  $additionalfirewallScripts = get-childitem $additionalfirewallScriptsFolder -Filter *.ps1
  foreach ($additionalfirewallScript in $additionalfirewallScripts){
      $rName=$additionalfirewallScript.name
      $rName=$rName.Substring(0,$rName.Length-4)
      $rep=Read-Host "Import $rName`?"
      if($rep -like 'Y'){
          . $additionalfirewallScript.FullName
      }
  }
} #importOptionalsRules

function checkFirewallRules(){
	$firewallRulesFolders=@()
	$firewallRulesFolders+="$ScriptsFolder\firewall\optionals"
	$firewallRulesFolders+="$ScriptsFolder\firewall"

	$firewallRulesFiles=@()
	foreach ($firewallRulesFolder in $firewallRulesFolders){
	  $firewallScripts = get-childitem $firewallRulesFolder -Filter *.ps1
	  foreach ($firewallScript in $firewallScripts){
			$firewallRulesFiles+=$firewallScript.fullname
		}
	}

	$fwRules=Get-NetFirewallRule
	foreach ($fwRule in $fwRules){
		foreach ($firewallRulesFile in $firewallRulesFiles){
			$fwRuleName=$fwRule.name
			$fwRuleDirection=$fwRule.direction
			$result = Get-Content $firewallRulesFile | Select-String "-name `"$fwRuleName`"" -quiet -casesensitive
			if (($result -eq $True)){
				break
			}
		}#foreach ($firewallRulesFile
		if (!($result -eq $True)){
			write-host "$fwRuleDirection '$fwRuleName' rule doesnt exist..."
			$saveFw=read-host "Save it?"
			if ($saveFw -eq "Y"){
				$saved=$false
				while ($saved -eq $false){
					foreach ($firewallRulesFile in $firewallRulesFiles){
						$fileName=Split-Path $firewallRulesFile -Leaf
						$save=read-host "Save '$fwRuleName' rule in the '$fileName'  file?"
						if ($save -eq 'Y'){
							addFirewallRule $fwRule $firewallRulesFile
						}
					}
					$exit=read-host "Try again?"
					if (! ($exit -eq 'Y')){
							$saved = $true
					}
				}#end while
			} else {
				Remove-NetFirewallRule -Name $fwRuleName
			}
		}
	} #foreach ($fwRule
} #checkFirewallRules


function addFirewallRule ($fwRule, $firewallRulesFile){
	$direction = $fwRule.Direction
	$d="-Direction $direction"
	$name = $fwRule.Name
	$n="-name `"$name`""
	$displayName = $fwRule.DisplayName
	$dn = "-displayname `"$displayName`""
	$group = $fwRule.Group
	$g="-Group `"$group`""
	$desc = $fwRule.description
	if ($desc){$de="-description `"$desc`""}else{$de=""}
	$enabled = $fwRule.Enabled
	if ($enabled){$en="-enabled `"$enabled`""}else{$en=""}
	$program = $fwRule.program
	if ($program){$p="-program `"$program`""}else{$p=""}
	$service = $fwRule.service
	if ($service){$s="-service `"$service`""}else{$s=""}
	$protocol = $fwRule.protocol
	if ($protocol){$pr="-protocol `"$protocol`""}else{$pr=""}
	$port = $fwRule.port
	if ($port){$prt="-RemotePort `"$port`""}else{$prt=""}

	$s="New-NetFirewallRule $d $n $dn $g $de $en $p $s $pr $prt -ErrorAction SilentlyContinue | out-null"
	Add-Content -Path $firewallRulesFile -Value "`r`n$s"
}
