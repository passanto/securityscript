#update "portable_update_intialized" property
function updatePU($phase, $boolean){
	#update properties file.phase
	$prop=".\import\Conf\securityScript.properties"
	$processFileText = "$phase=$boolean"
	$newProp = Get-Content $prop | ForEach-Object{
		$_ -replace "^$phase=.*$",$processFileText
	}
	$newProp | Set-Content $prop
}

function initializePU{
	try{
		#copy TO usb
		Write-Host -ForegroundColor Yellow "Insert a USB drive onto which copy the Portable Update folder."
		Write-Host -ForegroundColor Yellow "The USB drive needs to have a reasonable amount of free space on it."
		Read-Host 'Press Enter when inserted the drive...'
		$systemDrive=${env:systemdrive}
		$drives = GET-WMIOBJECT -class win32_logicaldisk
		$loopDrives = $true
		while ($loopDrives){
			foreach ($drive in $drives){
				if ($drive.DeviceID -ne $systemDrive){
					$driveId=$drive.DeviceID
					$driveIdChoosen=Read-Host -Prompt "Copy on $driveId ?"
					if ($driveIdChoosen -eq 'Y' -or $driveIdChoosen -eq 'y'){
						$loopDrives=$false
						$localSoftwaresFolder = (Get-Item .\import\Softwares\)
						$localPortUpFolder = (Get-Item .\import\Softwares\PortUp\)
						copy-item -path $localPortUpFolder -destination $driveId"\" -recurse -force
						Write-Host
						Write-Host -ForegroundColor Yellow "Portable Updated folder has been copied to $driveId."
						Read-Host "Press Enter to eject the $driveId drive"
						$vol = get-wmiobject -Class Win32_Volume | where{$_.DriveLetter -eq $driveId}
						$Eject =  New-Object -comObject Shell.Application
						$Eject.NameSpace(17).ParseName($vol.driveletter).InvokeVerb("Eject")
						Write-Host -ForegroundColor Yellow "The Portable Updated drive can now be moved to a system connected to the network."
						Write-Host -ForegroundColor red "On the connected system, launch PortUp.exe and press START to initialize it."
						updatePU "portable_update_intialized" "true"
						break
					}
				}
			}
			if ($loopDrives){
				write-host ""
				write-host "No exter drive selected."
				$res = read-host "Continue anyway?"
				if ($res -like 'Y'){
					$loopDrives=$false
				}
			}
		}
	} catch {
		Write-Warning $_
	}
}


function scanAndDownloadPU{
	try {
		#run from usb
		Write-Host -ForegroundColor Yellow "Insert the USB drive with the initialized PortableUpdate on it."
		Write-Host -ForegroundColor Yellow "Leave the drive inserted till scan completion."
		Write-Host
		Read-Host 'Press Enter when initialized Portable Updated drive has been inserted'
		$systemDrive=${env:systemdrive}
		$drives = GET-WMIOBJECT -class win32_logicaldisk
		$loopDrives = $true
		while ($loopDrives -eq $true){
			:loopdrives foreach ($drive in $drives){
				if ($drive.DeviceID -ne $systemDrive){
					$driveId=$drive.DeviceID
					$driveIdChoosen=Read-Host -Prompt "Is $driveId the drive with initialized PortableUpdate?"
					if ($driveIdChoosen -eq 'Y' -or $driveIdChoosen -eq 'y'){
						$externalPUFolderFound=$false
						while ($externalPUFolderFound -eq $false){
							try{
								$extFolders=get-childitem -path $driveId -Recurse -Directory
							} catch {
								continue loopdrives
							}
							:loopfolders foreach ($extFolder in $extFolders){
								if ($extFolder.Name -eq "PortUp"){
									$folderFullName=$extFolder.fullname
									Write-Host -ForegroundColor Yellow "Found $folderFullName on the drive drive."
									$confirmPUFolderFound = Read-Host "Confirm it's the updated folder? (Y/N)"
									if ($confirmPUFolderFound -eq 'Y' -or $confirmPUFolderFound -eq 'y'){
										$loopDrives=$false
										$externalPUFolderFound=$true
										performScanDownload $folderFullName $driveId
										break loopdrives
									}
									if ($confirmPUFolderFound -eq 'N' -or $confirmPUFolderFound -eq 'n'){
										continue loopfolders
									}
								}
							}
						}
					}
					#break
				}
			}
			write-host ""
		}
	} catch {
		Write-Warning $_
	}
}

function performScanDownload($folderFullName, $driveId){
	#copy-item -path $extFolder -destination $localSoftwaresFolder -recurse -force
	Write-Host
	Write-Host -foregroundcolor yellow 'Will now scan the system with Portable Updated'
	Write-Host -foregroundcolor red 'Go to the SEARCH tab and click START for start scanning'
	Write-Host -foregroundcolor yellow 'When completed, the "SEARCH RESULTS" pop-up will appear'
	Write-Host
	Write-Host -foregroundcolor red 'Portable Updated needs to be closed once completed'
	Read-Host 'Press Enter to launch Portable Updated (can take several minutes or even hours)'
	#$puExe = $localPortUpFolder+"\PortUp.exe"
	$puExe = $folderFullName+"\PortUp.exe"
	start-process $puExe -nonewwindow -wait
	Read-Host 'Press Enter when Portable Updated have scanned the system'
	#updatePU("portable_update_scanned", $true)
	Read-Host "Press Enter to eject the $driveId drive"
	try{
		$vol = get-wmiobject -Class Win32_Volume | where{$_.DriveLetter -eq $driveId}
		$Eject =  New-Object -comObject Shell.Application
		$Eject.NameSpace(17).ParseName($vol.driveletter).InvokeVerb("Eject")
	} catch {
		# log this
		Write-Host "Unable to eject the drive."
		Write-Host "Make sure the drive has been removed correctly."
		Read-Host 'Press Enter to continue...'
		Write-Host
	}
	Write-Host -ForegroundColor Yellow "The Portable Updated drive can now be moved to a system connected to the network."
	Write-Host
	Write-Host -ForegroundColor red "On the connected system, launch PortUp.exe."
	Write-Host -ForegroundColor red "Click on the SEARCH tab and then START."
	Write-Host
	Write-Host -ForegroundColor Yellow "Once scan is completed, click on the DOWNLOAD tab and select all the listed downloads."
	Write-Host -ForegroundColor red "Click START to begin the downloads."
	Write-Host
	Write-Host -ForegroundColor Yellow "When completed, exit and eject the drive from the external system."
	Write-Host
	Read-Host 'Press Enter to continue...'
	updatePU "portable_update_downloaded" "true"
}


function completePU{
	try {
		#copy FROM usb
		Read-Host 'Press Enter when the drive with Portable Updated downloaded files has been inserted'
		$systemDrive=${env:systemdrive}
		$drives = GET-WMIOBJECT -class win32_logicaldisk
		$loopDrives = $true
		while ($loopDrives){
			foreach ($drive in $drives){
				if ($drive.DeviceID -ne $systemDrive){
					$driveId=$drive.DeviceID
					$driveIdChoosen=Read-Host -Prompt "Is $driveId the drive with downloaded complete Portable Update folder on it?"
					if ($driveIdChoosen -eq 'Y' -or $driveIdChoosen -eq 'y'){
						$loopDrives=$false
						break
					}
				}
			}
			write-host ""
		}
		$externalPUFolderFound=$false
		while ($externalPUFolderFound -eq $false){
			$extFolders=get-childitem -path $driveId -Recurse -Directory -filter $folderName
			foreach ($extFolder in $extFolders){
				if ($extFolder.Name -eq "PortUp"){
					$folderFullName=$extFolder.fullname
					Write-Host -ForegroundColor Yellow "Found $folderFullName on the drive."
					$confirmPUFolderFound = Read-Host "Confirm it's the updated folder? (Y/N)"
					if ($confirmPUFolderFound -eq 'Y' -or $confirmPUFolderFound -eq 'y'){
						$externalPUFolderFound=$true
						#copy-item -path $extFolder -destination $localSoftwaresFolder -recurse -force
						Write-Host
						Write-Host -foregroundcolor yellow 'Will now install the required updates on this system'
						Write-Host -foregroundcolor red 'Go to the SEARCH tab and click START for start scanning'
						Write-Host
						Write-Host -foregroundcolor yellow 'When completed the "SEARCH RESULTS" pop-up will appear'
						Write-Host -foregroundcolor red 'Go to the INSTALL tab and click START for start installing the required updates'
						Write-Host
						Read-Host 'Press Enter to launch Portable Updated'
						#$puExe = $localPortUpFolder+"\PortUp.exe"
						$puExe = $folderFullName+"\PortUp.exe"
						start-process $puExe -nonewwindow -wait
						Write-Host -ForegroundColor Yellow "Update completed."
						Write-Host "Mind that the $folderFullName folder is the one with all the available downloads."
						Read-Host 'Press Enter to continue...'
						break
					}
				}
			}
		}
		updatePU "portable_update_completed" "true"
		updatePU "portable_update_required" "false"
	} catch {
		Write-Warning $_
	}
}

# useful?
function resetPuProcess{
	try {
		updatePU "external_system_available" "false"
		updatePU "portable_update_intialized" "false"
		updatePU "portable_update_scanned" "false"
		updatePU "portable_update_downloaded" "false"
		updatePU "portable_update_completed" "false"
	} catch {
		Write-Warning $_
		errorsLog $_
		Write-Host -BackgroundColor Yellow -ForegroundColor Yellow "Something went wrong while resetting the Offline Update process"
		Write-Host -ForegroundColor Red "Please check the script."
		Write-Host -BackgroundColor Yellow -ForegroundColor Yellow "Will now exit... "
		$continue = Read-Host "Press enter to exit"
		exit
	}
}
