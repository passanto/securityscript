####Hardware requirements:
-external hard drive (file history backup)  
-external thumb drive (hard disk encryption key if no TPM; backup for TPM; baselines)  
-external system connect to the internet (for intialize and download disconnected updates)  


####Instructions
For a fresh installed system, it is suggested to remain offline until prompted by the process.  

At least two users have to be created.  
-A standard user, for the regular activities  
--An administrator user for the maintenance tasks  

Copy the root folder of the script (\SecurityProcess) in the root of the syste drive (ie: "C:\").  
For the first execution, launch the run.bat file as administrator.  
When required the script will auto reboot the machine and auto restart itself.  


The folder can be kept for future use (ie after a Windows update).  


####Process sections:  
-prerequisites  
-stable system settings  
-network  
-exposed surface reduction  
-functionalities  
-live update  
-further system settings  
-data protection  
-browser settings  
-credential settings  
-physical security  
-system audit and tracing  
-system image and backup  


####Configuration
Edit the following files for customization:  

Custom events views:  
./Conf/custom_events/  

Services to disable:  
./Conf/services2disable.txt  

Windows 10 Apps to remove:  
./Conf/unwantedApps.txt  

Paths to whitelist/blacklist:  
./Conf/SRP/custom/  



####External links:  
Windows 10 Media Creation Tool: http://go.microsoft.com/fwlink/?LinkId=691209
